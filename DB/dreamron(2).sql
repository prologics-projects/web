-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 24, 2021 at 05:00 AM
-- Server version: 5.7.31
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dreamron`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
CREATE TABLE IF NOT EXISTS `albums` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hits` int(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `optional1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `name`, `description`, `image`, `hits`, `status`, `optional1`, `created_at`, `updated_at`) VALUES
(7, 'Production and Laboratories', NULL, 'album_cover/1615907180Dreamron.JPG', 0, NULL, NULL, '2021-03-16 09:36:20', '2021-05-17 01:09:19');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE IF NOT EXISTS `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `category_id`, `image`, `description`, `content`, `author`, `title`, `status`, `published_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 4, 'posts/F4dvhkxuMPmtBLXxPwl8Rc7ntspI8UX9Z5QnmOOx.png', 'Short Description   Short Description   Short Description   Short Description', '<p>Post Content Post Content Post Content Post Content Post Content Post Content Post ContentPost Content Post Content Post ContentPost ContentPost ContentPost Content Post Content Post Content Post ContentPost ContentPost Content Post Content</p>', 'Author', 'Test title Test title Test titleTest title Test title Test title Test title', 1, '2021-11-08 18:30:00', '2021-11-09 05:32:11', '2021-11-09 05:32:11', NULL),
(4, 4, 'posts/F4dvhkxuMPmtBLXxPwl8Rc7ntspI8UX9Z5QnmOOx.png', 'Short Description   Short Description   Short Description   Short Description', '<p>Post Content Post Content Post Content Post Content Post Content Post Content Post ContentPost Content Post Content Post ContentPost ContentPost ContentPost Content Post Content Post Content Post ContentPost ContentPost Content Post Content</p>', 'Author', 'Test title Test title Test titleTest title Test title Test title Test title', 1, '2021-11-08 18:30:00', '2021-11-09 05:32:11', '2021-11-09 05:32:11', NULL),
(6, 4, 'posts/F4dvhkxuMPmtBLXxPwl8Rc7ntspI8UX9Z5QnmOOx.png', 'Short Description   Short Description   Short Description   Short Description', '<p>Post Content Post Content Post Content Post Content Post Content Post Content Post ContentPost Content Post Content Post ContentPost ContentPost ContentPost Content Post Content Post Content Post ContentPost ContentPost Content Post Content</p>', 'Author', 'Test title Test title Test titleTest title Test title Test title Test title', 1, '2021-11-08 18:30:00', '2021-11-09 05:32:11', '2021-11-09 05:32:11', NULL),
(7, 4, 'posts/F4dvhkxuMPmtBLXxPwl8Rc7ntspI8UX9Z5QnmOOx.png', 'Short Description   Short Description   Short Description   Short Description', '<p>Post Content Post Content Post Content Post Content Post Content Post Content Post ContentPost Content Post Content Post ContentPost ContentPost ContentPost Content Post Content Post Content Post ContentPost ContentPost Content Post Content</p>', 'Author', 'Test title Test title Test titleTest title Test title Test title Test title', 1, '2021-11-08 18:30:00', '2021-11-09 05:32:11', '2021-11-09 05:32:11', NULL),
(8, 4, 'posts/F4dvhkxuMPmtBLXxPwl8Rc7ntspI8UX9Z5QnmOOx.png', 'Short Description   Short Description   Short Description   Short Description', '<p>Post Content Post Content Post Content Post Content Post Content Post Content Post ContentPost Content Post Content Post ContentPost ContentPost ContentPost Content Post Content Post Content Post ContentPost ContentPost Content Post Content</p>', 'Author', 'Test title Test title Test titleTest title Test title Test title Test title', 1, '2021-11-08 18:30:00', '2021-11-09 05:32:11', '2021-11-09 05:32:11', NULL),
(9, 4, 'posts/F4dvhkxuMPmtBLXxPwl8Rc7ntspI8UX9Z5QnmOOx.png', 'Short Description   Short Description   Short Description   Short Description', '<p>Post Content Post Content Post Content Post Content Post Content Post Content Post ContentPost Content Post Content Post ContentPost ContentPost ContentPost Content Post Content Post Content Post ContentPost ContentPost Content Post Content</p>', 'Author', 'Test title Test title Test titleTest title Test title Test title Test title', 1, '2021-11-08 18:30:00', '2021-11-09 05:32:11', '2021-11-09 05:32:11', NULL),
(10, 4, 'posts/F4dvhkxuMPmtBLXxPwl8Rc7ntspI8UX9Z5QnmOOx.png', 'Short Description   Short Description   Short Description   Short Description', '<p>Post Content Post Content Post Content Post Content Post Content Post Content Post ContentPost Content Post Content Post ContentPost ContentPost ContentPost Content Post Content Post Content Post ContentPost ContentPost Content Post Content</p>', 'Author', 'Test title Test title Test titleTest title Test title Test title Test title', 1, '2021-11-08 18:30:00', '2021-11-09 05:32:11', '2021-11-09 05:32:11', NULL),
(11, 4, 'posts/F4dvhkxuMPmtBLXxPwl8Rc7ntspI8UX9Z5QnmOOx.png', 'Short Description   Short Description   Short Description   Short Description', '<p>Post Content Post Content Post Content Post Content Post Content Post Content Post ContentPost Content Post Content Post ContentPost ContentPost ContentPost Content Post Content Post Content Post ContentPost ContentPost Content Post Content</p>', 'Author', 'Test title Test title Test titleTest title Test title Test title Test title', 1, '2021-11-08 18:30:00', '2021-11-09 05:32:11', '2021-11-09 05:32:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `image`, `about`, `created_at`, `updated_at`) VALUES
(2, 'Dreamron', NULL, NULL, '2020-12-31 09:07:44', '2020-12-31 09:07:44'),
(3, 'EVON', NULL, NULL, '2020-12-31 09:07:55', '2020-12-31 09:07:55'),
(4, 'Aurica', NULL, NULL, '2020-12-31 09:08:04', '2020-12-31 09:08:04'),
(5, 'Kleara', NULL, NULL, '2020-12-31 09:08:35', '2020-12-31 09:08:35');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Skin Care', '2020-12-31 09:09:01', '2020-12-31 09:09:01'),
(3, 'Hair Care', '2020-12-31 09:09:08', '2020-12-31 09:09:08'),
(4, 'Hair Colors', '2020-12-31 09:09:16', '2020-12-31 09:09:16'),
(5, 'Body Care', '2020-12-31 09:09:24', '2020-12-31 09:09:24'),
(6, 'Toiletries', '2020-12-31 09:09:33', '2020-12-31 09:09:33');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
CREATE TABLE IF NOT EXISTS `galleries` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `album_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `galleries_album_id_foreign` (`album_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `album_id`, `image`, `created_at`, `updated_at`) VALUES
(38, 7, '161590719838.JPG', '2021-03-16 09:36:38', '2021-03-16 09:36:38'),
(39, 7, '161590720039.JPG', '2021-03-16 09:36:39', '2021-03-16 09:36:40'),
(40, 7, '161590720040.JPG', '2021-03-16 09:36:40', '2021-03-16 09:36:40'),
(41, 7, '161590720241.JPG', '2021-03-16 09:36:42', '2021-03-16 09:36:42'),
(42, 7, '161590720442.JPG', '2021-03-16 09:36:44', '2021-03-16 09:36:44'),
(43, 7, '161590720543.JPG', '2021-03-16 09:36:45', '2021-03-16 09:36:45'),
(44, 7, '161590720644.JPG', '2021-03-16 09:36:46', '2021-03-16 09:36:46'),
(45, 7, '161590720945.JPG', '2021-03-16 09:36:48', '2021-03-16 09:36:49'),
(46, 7, '161590721246.JPG', '2021-03-16 09:36:51', '2021-03-16 09:36:52');

-- --------------------------------------------------------

--
-- Table structure for table `launcs`
--

DROP TABLE IF EXISTS `launcs`;
CREATE TABLE IF NOT EXISTS `launcs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `launcs`
--

INSERT INTO `launcs` (`id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, '2020-12-31 07:25:28');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_12_07_085956_create_brands_table', 1),
(4, '2020_12_07_090028_create_categories_table', 1),
(5, '2020_12_07_090056_create_sub_categories_table', 1),
(6, '2020_12_07_090112_create_products_table', 1),
(7, '2020_12_24_181028_create_news_table', 2),
(8, '2020_12_31_080440_create_launcs_table', 3),
(9, '2021_03_14_051629_create_albums_table', 4),
(10, '2021_03_14_053648_create_galleries_table', 4),
(11, '2021_11_05_102219_create_blogs_table', 5),
(12, '2021_11_09_060521_edit_table_blog', 6);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `subcategory_id` bigint(20) UNSIGNED DEFAULT NULL,
  `brand_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sku` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_category_id_foreign` (`category_id`),
  KEY `products_subcategory_id_foreign` (`subcategory_id`),
  KEY `products_brand_id_foreign` (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `subcategory_id`, `brand_id`, `name`, `sku`, `url`, `barcode`, `image`, `description`, `status`, `amount`, `created_at`, `updated_at`) VALUES
(9, 2, 6, 4, 'Aurica Whitening Day Cream', NULL, NULL, NULL, 'products/Day  Cream_1616041738.png', 'Aurica Whitening Day Cream with a unique herbal blend provides hydration, a lightening effect, and protection to your skin. \r\nIt helps to fade dark spots and blemishes to gives you even-tone skin. The non - greasy rich cream adds glow to the skin and protects the skin from drying and UV rays.', 1, NULL, '2021-03-17 22:57:25', '2021-03-17 23:01:16'),
(10, 2, 5, 4, 'Aurica Natural Glow Fairness Cream', NULL, NULL, NULL, 'products/Fairness Cream_1616042169.png', 'Aurica Natural Glow Fairness Cream lightens your skin and gives you radiant, even-toned skin. It helps to nourish and moisturize the skin and provides healthy skin. Protects the skin from the harmful UV rays to prevent skin darkening and damages. \r\nAvailable Pack Sizes - 25g and 50g', 1, NULL, '2021-03-17 23:05:10', '2021-03-17 23:06:10'),
(11, 2, 7, 4, 'Aurica Revitalizing Night Cream', NULL, NULL, NULL, 'products/Night Cream Post_1616044736.png', 'Aurica Revitalizing Night cream works overnight to nourish skin to renew it, repairing daily damages and dryness. It hydrates and fights the signs of aging, smoothing wrinkles, and improves the skin tone. Creamy texture keeps your skin soft, supple & rejuvenated.', 1, NULL, '2021-03-17 23:48:33', '2021-03-17 23:48:56'),
(12, 2, 9, 4, 'Aurica Anti Wrinkle Cream', NULL, NULL, NULL, 'products/Anti Wrinkle Cream_1616046619.png', 'Aurica Anti Wrinkle Cream with a blend of herbals is designed to reduce wrinkles and fine lines recovering lost skin firmness and elasticity. Revitalizing the properties of added ingredients helps to boost the natural process of the skin.', 1, NULL, '2021-03-18 00:19:58', '2021-03-18 00:21:10'),
(13, 2, 1, 4, 'Aurica Purifying Neem Face Wash', NULL, NULL, NULL, 'products/Neem_1616046846.png', 'Aurica Purifying Neem Face Wash is formulated with the hydrolyzed collagen and elastin, an effective natural anti-acne ingredient & skin conditioners. It deeply cleanses your skin, removing excess oil, sebum, and dirt without drying and controlling acne & pimples. It will reduce acne scars, dark patches and improve the healthiness and complexion of your skin.\r\nAvailable Pack sizes - 50 ml and 100 ml', 1, NULL, '2021-03-18 00:23:42', '2021-03-18 00:24:06'),
(14, 2, 1, 4, 'Aurica Fairness Saffron Face Wash', NULL, NULL, NULL, 'products/Saffron_1616047040.png', 'Aurica Fairness Saffron Face Wash gently cleanses the skin restoring moisture and skin\'s natural moisture and leaving skin soft smooth while brightening it. Also added saffron naturally tanned skin, hyperpigmentation, dark spots, and skin blemishes, giving an even-toned, radiant, and healthy skin.\r\nAvailable pack sizes - 50 ml and 100 ml', 1, NULL, '2021-03-18 00:27:04', '2021-03-18 00:27:20'),
(15, 2, 1, 4, 'Aurica Moisturizing Aloe Vera Face Wash', NULL, NULL, NULL, 'products/Aloe Vera_1616047243.png', 'Aurica Moisturizing Aloe Vera Face Wash is formulated with the extra hydrating mild formulation. Also, it gently cleanses the skin while restoring the moisturize and oil balance. Other added herbal extracts revitalize and rejuvenate the skin while reducing the dark circles and blemishes, making your skin healthier and brighter.', 1, NULL, '2021-03-18 00:30:25', '2021-03-18 00:30:43'),
(16, 2, 5, 3, 'Evon Vitamin C Cleansing Cream', NULL, NULL, NULL, 'products/Vitamin C_1616047786.png', 'Evon Vitamin C Cleansing Cream enriched with pure orange extract with rich vitamin C. It removes embedded dirt, excess oil, makeup effectively, and giving gentle cleansing to your skin. This cleanser contains small beads and its wraps in dead skin cells removing and giving refreshed skin. It will help you to nourish, hydrate, and brighten skin, dark spots, and deep pigmentation areas on the skin. Regular use will stimulate and regenerate the skin tissues, reduced wrinkles, enhance skin tone and maintain soft and supple skin.', 1, NULL, '2021-03-18 00:39:21', '2021-03-18 00:39:46'),
(17, 2, 1, 3, 'Evon Cucumber Facial Wash 180ml', NULL, NULL, NULL, 'products/Cucumber Face Wash_1616048050.png', 'Evon Cucumber Facial Wash 180ml will help you to washes away all the excess oil, traces of makeup, and impurities. It is a 100% soap-free, PH-balanced formula. Cucumber extract restores natural moisture and prevents drying skin. It revitalizes your skin and makes it younger, radiant, fresh, and healthy.', 1, NULL, '2021-03-18 00:43:36', '2021-03-18 00:44:10'),
(18, 2, 10, 3, 'Evon Cucumber Cleansing Cream', NULL, NULL, NULL, 'products/Cucumber Cleanser_1616048223.png', 'Evon Cucumber Cleansing Cream is enriched with cucumber extract that hydrates, balance the oil content, and revitalize the skin.', 1, NULL, '2021-03-18 00:46:44', '2021-03-18 00:47:03'),
(19, 2, 2, 3, 'Evon Cucumber Facial Scrub', NULL, NULL, NULL, 'products/Cucumber Facial Scrub_1616048516.png', 'Evon Cucumber Facial Scrub with walnut shell powder granules gently exfoliates dead skin cells and skin impurities, giving smooth, refresh, and soft skin. Added cucumber extract restored natural moisture, leaving your skin healthy. It will help to reduce blackheads, whiteheads, blemishes and gives you radiant skin.', 1, NULL, '2021-03-18 00:51:37', '2021-03-18 00:51:56'),
(20, 3, 11, 3, 'Evon Hair Massage Tonic', NULL, NULL, NULL, 'products/Hair Tonic_1616048890.png', 'Evon Hair Massage Tonic gives a gentle cooling effect to the scalp while massaging, and it also reduces mental stresses and fatigue. It helps reducing hair from drying and reduce the appearance of dandruff. Added castor oil strengthens the hair and enhances the black color hair and leaving it strong, healthy.', 1, NULL, '2021-03-18 00:57:44', '2021-03-18 00:58:10'),
(21, 3, 12, 3, 'Evon Leave - On Silicone Conditioner', NULL, NULL, NULL, 'products/Leave On Conditioner_1616049296.png', 'Evon Leave - On Silicone Conditioner 100ml enriched with vitamin E and wheat protein. Wheat protein conditions and repairs hair inside out, making hair healthy. Vitamin E protects your hair from drying and damaging. Also, it gives soft and silky hair. Enhance the natural color of hair and repair split ends and damages.', 1, NULL, '2021-03-18 01:04:37', '2021-03-18 01:04:56'),
(22, 4, 19, 3, 'Evon Permanent Hair Color Pack', NULL, NULL, NULL, 'products/Hair Color_1616049585.png', 'Evon Permanent Hair Colors provide long-lasting colors with conditions formula enriched with avocado oil, and vitamin c nourishes hair without drying. Less ammonia, 100% grey coverage, and even coloring, root to top.', 1, NULL, '2021-03-18 01:08:52', '2021-03-18 01:09:45'),
(23, 3, 13, 2, 'Dreamron Anti-Breakage Shampoo', NULL, NULL, NULL, 'products/CS4A8699_1621240377.png', 'Specifically designed to provide high damage resistance and strengthen hair against breakage and spilt ends. Rich in Grape Seed extract, a natural UV protector, and Ginseng extract that nourishes the roots and prevents hair loss. This unique formula is also equipped with Vitamin E and Pro-Vitamin B5 to retain moisture and give you healthy, soft & shiny hair.', 1, NULL, '2021-05-17 03:02:11', '2021-05-17 03:02:57'),
(24, 3, 14, 2, 'Dreamron Anti-Breakage Conditioner', NULL, NULL, NULL, 'products/CS4A9062_1621240512.png', 'Strengthens the hair giving it high damage resistance by restoring natural moisture and oil content. Active ingredients Grape seed extract acts as a natural UV protector, Ginseng extract nourishes, retains moisture, leaving you with strong and healthy hair without breakages or split ends.', 1, NULL, '2021-05-17 03:04:54', '2021-05-17 03:05:12');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

DROP TABLE IF EXISTS `sub_categories`;
CREATE TABLE IF NOT EXISTS `sub_categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sub_categories_category_id_foreign` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 2, 'Face Wash', '2021-03-17 22:53:46', '2021-03-17 22:53:46'),
(2, 2, 'Facial Scrub', '2021-03-17 22:54:02', '2021-03-17 22:54:02'),
(3, 2, 'Skin Toner', '2021-03-17 22:54:18', '2021-03-17 22:54:18'),
(4, 2, 'Facial Pack', '2021-03-17 22:54:43', '2021-03-17 22:54:43'),
(5, 2, 'Fairness Cream', '2021-03-17 22:55:11', '2021-03-17 22:55:11'),
(6, 2, 'Day Cream', '2021-03-17 22:55:25', '2021-03-17 23:02:18'),
(7, 2, 'Night Cream', '2021-03-17 23:02:33', '2021-03-17 23:02:33'),
(8, 2, 'Anti-Acne Cream', '2021-03-17 23:02:59', '2021-03-17 23:02:59'),
(9, 2, 'Anti Wrinkle Cream', '2021-03-18 00:20:47', '2021-03-18 00:20:47'),
(10, 2, 'Cleansing cream', '2021-03-18 00:31:59', '2021-03-18 00:31:59'),
(11, 3, 'Hair Tonics', '2021-03-18 00:32:15', '2021-03-18 00:32:15'),
(12, 3, 'Silicon Hair Treatment Tonics', '2021-03-18 00:32:57', '2021-03-18 00:32:57'),
(13, 3, 'Shampoo', '2021-03-18 00:33:31', '2021-03-18 00:33:31'),
(14, 3, 'Conditioner', '2021-03-18 00:33:47', '2021-03-18 00:33:47'),
(15, 2, 'Anti-Dandruff Shampoo', '2021-03-18 00:34:26', '2021-03-18 00:34:26'),
(16, 4, 'Fashion Colours', '2021-03-18 00:34:46', '2021-03-18 00:34:46'),
(17, 4, 'Five Min. Hair Colors', '2021-03-18 00:35:31', '2021-03-18 00:35:31'),
(18, 4, 'Henna Based Colors', '2021-03-18 00:36:17', '2021-03-18 00:36:17'),
(19, 4, 'Permanent Hair Color Pack', '2021-03-18 01:06:20', '2021-03-18 01:07:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `about`, `status`, `role`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Prologics Admin', 'Main User', '1', 'Admin', 'admin@gmail.com', NULL, '$2y$10$6CpCZeB2VM6NOs06h4l7w.q8laMlCVah8JyMG3NZ0LtuJVqj4GpSm', NULL, '2020-12-07 06:10:33', '2020-12-07 06:10:33');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `galleries`
--
ALTER TABLE `galleries`
  ADD CONSTRAINT `galleries_album_id_foreign` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `products_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `sub_categories` (`id`);

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
