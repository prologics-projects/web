var video = document.getElementById("bg-video");
var muteNot = document.getElementById("muteOrNot");
var playPause = document.getElementById("playPauseBtn");

function scrollToTop() {
    document.getElementById("expander").classList.add("to-top");
    if (video.paused) {
        video.paused();
    } else {
        video.pause();
    }
}

function playPauseController() {
    if (video.paused) {
        video.play();
        playPause.className = "video-control fa fa-pause-circle";
    } else {
        video.pause();
        playPause.className = "video-control fa fa-play-circle";
    }
}

function muteOrNot() {
    if (video.muted === true) {
        video.muted = false;
        muteNot.className = "volume-control fa fa-volume-up";
    }
    else if (video.muted === false) {
        video.muted = true;
        muteNot.className = "volume-control fa fa-volume-mute";
    }
}

video.onended = function(e) {
    document.getElementById("expander").classList.add("to-top");
};

function goBack() {
    window.history.back();
}
