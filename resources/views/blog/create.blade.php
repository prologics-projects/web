@extends('layouts.app')

@section('content')


        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between g-3">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title page-title">{{ isset($blog)?'Update':'Add' }} Post</h2>
                    <div class="nk-block-des text-soft">
                        * are required.
                    </div>
                </div>
                
                <div class="nk-block-head-content">
                    <ul class="nk-block-tools g-3">
                        <li class="nk-block-tools-opt">
                            <a href="{{ route('blog.index') }}" class="btn btn-primary">
                                <em class="icon ni ni-arrow-left"></em>
                                <span>Back</span>
                            </a>
                        </li>
                    </ul>
                </div><!-- .nk-block-head-content -->
            </div><!-- .nk-block-head -->
        </div><!-- .nk-block-head -->
            
        @include('partials.session')
        @include('partials.error')
        
        <div class="nk-block nk-block-lg">
            <div class="card card-bordered">
                <div class="card-inner">
                    <div class="card-head">
                        <h5 class="card-title">New Post Setup</h5>
                    </div>
                    <form action="{{ isset($blog)? route('blog.update',$blog->id):route('blog.store') }}" class="gy-3" class="is-alter form-validate" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @if (isset($blog))
                            {{ method_field('PUT') }}
                        @endif
                        <div class="row g-4">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Action Category *</label>
                                    <div class="form-control-wrap">
                                        <select class="form-select form-control form-control-lg" name="category" id="category" data-search="on">
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}"
                                                    @if (isset($post))
                                                        @if ($category->id==$post->category_id)
                                                            selected
                                                        @endif
                                                    @endif
                                                >{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label" for="image">Cover *</label>
                                    <div class="form-control-wrap">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="image" id="image" value="{{ isset($blog)?$blog->image:old('image')}}">
                                            <label class="custom-file-label" for="image">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label" for="title">Action Title *</label>
                                    <div class="form-control-wrap">
                                        <input type="text" class="form-control" name="title" id="title" value="{{ isset($blog)?$blog->title:old('title') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label" for="description">Short Description *</label>
                                    <div class="form-control-wrap">
                                        <textarea class="form-control"  name="description" id="description" cols="30" rows="2">
                                            {{ isset($blog)?$blog->description:old('description')}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label" for="pay-amount-1">Post Content *</label>
                                    <div class="form-control-wrap">
                                        <textarea class="form-control"  name="content" id="content" cols="30" rows="10">
                                            {{ isset($blog)?$blog->content:old('content') }}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label" for="author">Author *</label>
                                    <div class="form-control-wrap">
                                        <input type="text" class="form-control" name="author" id="author"  value="{{ isset($blog)?$blog->author:'Dreamron' }}" value="{{ old('author') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label" for="published_at">Published Date *</label>
                                    <div class="form-control-wrap">
                                        <input type="text" name="published_at" id="published_at" data-date-format="yyyy-mm-dd" class="form-control date-picker" value="{{ isset($blog)?$blog->published_at:'' }}">
                                    </div>
                                </div>
                            </div>

                            @if (isset($blog))
                            <div class="form-group">
                                <img src="{{ asset('storage/'.$blog->image) }}" style="width:100%">
                            </div>
                            @endif

                    
                            <div class="col-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-lg btn-primary"><em class="icon ni ni-save"></em><span>Save</span> </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- .nk-block -->
@endsection

@section('script')
    <script type="text/javascript">
        CKEDITOR.replace('content', {
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
@endsection

@section('css')

    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
@endsection