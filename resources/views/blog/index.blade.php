@extends('layouts.app')

@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between g-3">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">Blog Posts</h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{$blogs->count()}} posts.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <ul class="nk-block-tools g-3">
                    <li>
                        <div class="drodown">
                            
                            <a href="{{ route('blog.create') }}" class="btn btn-primary">
                                <em class="icon ni ni-plus"></em>
                                <span>Add New Action</span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->

    
    @include('partials.error')
    
    @include('partials.session')
    
    <div class="nk-block nk-block-lg">
        <div class="card card-preview">
            <div class="card-inner">
                <table class="datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="false" id="blogs">
                    <thead>
                        <tr class="nk-tb-item nk-tb-head">
                            <th class="nk-tb-col "><span class="sub-text">#</span></th>
                            <th class="nk-tb-col"><span class="sub-text">Title</span></th>
                            <th class="nk-tb-col tb-col-mb"><span class="sub-text">Category</span></th>
                            <th class="nk-tb-col tb-col-lg"><span class="sub-text">Created at</span></th>
                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></th>
                            <th class="nk-tb-col nk-tb-col-tools text-right">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i=1;
                        @endphp
                        @foreach ($blogs->reverse() as $blog)
                            <tr class="nk-tb-item">
                                <td class="nk-tb-col tb-col-md">
                                    <span>{{ $i }}</span>
                                </td>
                                <td class="nk-tb-col">
                                    <div class="user-card">
                                        <div class="user-avatar bg-dim-primary d-none d-sm-flex">
                                            <img src="{{ asset('storage/'.$blog->image) }}" style="overflow: hidden; width:50px; height:50px" alt="">
                                        </div>
                                        <div class="user-info">
                                            <span class="tb-lead">{{ substr($blog->title,0,25) }} <span class="dot dot-success d-md-none ml-1"></span></span>
                                            <span>Action</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="nk-tb-col tb-col-md">
                                    <span>{{ $blog->category_id }}</span>
                                </td>
                                <td class="nk-tb-col tb-col-md">
                                    <span>{{ date('d M Y', strtotime($blog->published_at)) }}</span>
                                </td>
                                <td class="nk-tb-col tb-col-md">
                                    @if ($blog->status=='1')
                                        <span class="tb-status text-success">Published
                                    @else
                                        <span class="tb-status text-warning">Draft
                                    @endif </span>
                                </td>
                                <td class="nk-tb-col nk-tb-col-tools">
                                    <ul class="nk-tb-actions gx-1">
                                        <li>
                                            <div class="drodown">
                                                <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <ul class="link-list-opt no-bdr">
                                                        <li><a href="{{ route('blog.edit',$blog->id) }}"><em class="icon ni ni-eye-alt"></em><span>Edit</span></a></li>
                                                        <li><a role="button" onclick="handleDelete({{$blog->id}})"><em class="icon ni ni-trash"></em><span>Remove</span></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </td>
                            </tr><!-- .nk-tb-item  -->
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- .card-preview -->
    </div> <!-- nk-block -->

    <!-- Modal Alert -->
    <div class="modal fade" tabindex="-1"  role="dialog" id="deleteModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="" method="post" id="deleteform">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
                <div class="modal-content">
                    <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-alert bg-warning"></em>
                            <h4 class="nk-modal-title">Delete Action!</h4>
                            <div class="nk-modal-text">
                                <div class="caption-text">Are you sure, you want to <strong>delete</strong> this action ?</div>
                                <span class="sub-text-sm">This action can not be revert.</span>
                            </div>
                            <div class="nk-modal-action">
                                <button type="button" class="btn-lg btn-mw btn-primary" data-dismiss="modal">No, Go back</button>
                                <button type="submit" class="btn-lg btn-mw btn-danger">Yes, Delete</button>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function handleDelete(id){
            var form= document.getElementById('deleteform')
            form.action='/blog/'+id
            $('#deleteModal').modal('show')
        }

    </script> 
@endsection