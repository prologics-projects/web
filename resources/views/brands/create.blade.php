@extends('layouts.app')

@section('content')


        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between g-3">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title page-title">{{ isset($brand)?'Update':'Add' }} Brand</h2>
                    <div class="nk-block-des text-soft">
                        * are required.
                    </div>
                </div>
                
                <div class="nk-block-head-content">
                    <ul class="nk-block-tools g-3">
                        <li class="nk-block-tools-opt">
                            <a href="{{ route('brands.index') }}" class="btn btn-primary">
                                <em class="icon ni ni-arrow-left"></em>
                                <span>Back</span>
                            </a>
                        </li>
                    </ul>
                </div><!-- .nk-block-head-content -->
            </div><!-- .nk-block-head -->
        </div><!-- .nk-block-head -->
            
        @include('partials.session')
        @include('partials.error')
        
        <div class="nk-block nk-block-lg">
            <div class="card card-bordered">
                <div class="card-inner">
                    <div class="card-head">
                        <h5 class="card-title">{{ isset($brand)?'Edit':'New' }} Brand Setup</h5>
                    </div>
                    <form action="{{ isset($brand)? route('brands.update',$brand->id):route('brands.store') }}" class="gy-3" class="is-alter form-validate" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @if (isset($brand))
                            {{ method_field('PUT') }}
                        @endif
                        <div class="row g-4">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label" for="name">Brand Name *</label>
                                    <div class="form-control-wrap">
                                        <input type="text" class="form-control" name="name" id="name" value="{{ isset($brand)?$brand->name:old('name') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label" for="image">Logo</label>
                                    <div class="form-control-wrap">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="image" id="image" value="{{ isset($brand)?$brand->image:old('image')}}">
                                            <label class="custom-file-label" for="image">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label" for="about">About Brands</label>
                                    <div class="form-control-wrap">
                                        <textarea class="form-control"  name="about" id="about" cols="30" rows="2">{{ isset($brand)?$brand->about:old('about')}}</textarea>
                                    </div>
                                </div>
                            </div>

                            @if (isset($brand->image))
                                <div class="form-group">
                                    <img src="{{ asset('storage/'.$brand->image) }}" style="width:100%">
                                </div>
                            @endif

                    
                            <div class="col-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-lg btn-primary"><em class="icon ni ni-save"></em><span>Save & Next</span> </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- .nk-block -->
@endsection

@section('script')
    <script type="text/javascript">
        CKEDITOR.replace('content', {
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
@endsection

@section('css')
    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
@endsection