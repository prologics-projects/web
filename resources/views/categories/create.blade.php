@extends('layouts.app')

@section('content')


        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between g-3">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title page-title">{{ isset($category)?'Update':'Add' }} Category</h2>
                    <div class="nk-block-des text-soft">
                        * are required.
                    </div>
                </div>
                
                <div class="nk-block-head-content">
                    <ul class="nk-block-tools g-3">
                        <li class="nk-block-tools-opt">
                            <a href="{{ route('categories.index') }}" class="btn btn-primary">
                                <em class="icon ni ni-arrow-left"></em>
                                <span>Back</span>
                            </a>
                        </li>
                    </ul>
                </div><!-- .nk-block-head-content -->
            </div><!-- .nk-block-head -->
        </div><!-- .nk-block-head -->
            
        @include('partials.session')
        @include('partials.error')
        
        <div class="nk-block nk-block-lg">
            <div class="card card-bordered">
                <div class="card-inner">
                    <div class="card-head">
                        <h5 class="card-title">{{ isset($category)?'Edit':'New' }} Category Setup</h5>
                    </div>
                    <form action="{{ isset($category)? route('categories.update',$category->id):route('categories.store') }}" class="gy-3" class="is-alter form-validate" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @if (isset($category))
                            {{ method_field('PUT') }}
                        @endif
                        <div class="row g-4">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label" for="name">Category Name *</label>
                                    <div class="form-control-wrap">
                                        <input type="text" class="form-control" name="name" id="name" value="{{ isset($category)?$category->name:old('name') }}">
                                    </div>
                                </div>
                            </div>

                    
                            <div class="col-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-lg btn-primary"><em class="icon ni ni-save"></em><span>Save & Next</span> </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- .nk-block -->
@endsection

@section('script')
    <script type="text/javascript">
        CKEDITOR.replace('content', {
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
@endsection

@section('css')
    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
@endsection