
    <!--footer-->
    <div class="main-footer">
        
        {{-- <div class="main-progress-bar animate-brand">
            <div class="brand-bar bar-left"></div>
            <div class="brand-bar bar-right"></div>
        </div> --}}
        
        
        <ul class="social-icons">
            <li>
                <a href="https://www.facebook.com/dreamronsrilanka/" target="_blank">
                    <i class="fa fa-facebook"></i>
                </a>
            </li>
            <li>
                <a href="https://www.instagram.com/dreamronsrilanka" target="_blank">
                    <i class="fa fa-instagram"></i>
                </a>
            </li>
            <li>
                <a href="#" target="_blank">
                    <i class="fa fa-twitter"></i>
                </a>
            </li>
        </ul>
    </div>
    <!--/.footer-->