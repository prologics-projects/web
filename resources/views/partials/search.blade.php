
<div class="global-search">
    <div class="global-search-inner">
        <form action="{{ route('search') }}" method="POST" id="searchform">
            {{ csrf_field() }}
            <i class="fas fa-search" id="searchsubmit" aria-hidden="true" onclick="document.getElementById('searchform').submit();"></i>
            <input type="text" name="searchtext" class="form-control">
        </form>
    </div>
</div>