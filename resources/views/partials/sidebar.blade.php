
<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto flex-column">
        <li class="nav-item">
            <a class="nav-link" id="home" href="{{ route('homepage') }}">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="story" href="{{ route('story') }}">Our Story</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="products" href="{{ route('product') }}">Products</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="randd" href="{{ route('rnd') }}">Production & Laboratories</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="oem" href="{{ route('oem') }}">Contract Manufacturing</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="galleryshow" href="{{ route('galleryshow') }}">Gallery</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="contact" href="{{ route('contact') }}">Contact</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="blogs" href="{{ route('blogs') }}">News Room</a>
        </li>
    </ul>
</div>