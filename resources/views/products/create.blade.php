@extends('layouts.app')


@section('content')


        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between g-3">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title page-title">{{ isset($product)?'Update':'Add' }} Product</h2>
                    <div class="nk-block-des text-soft">
                        * are required.
                    </div>
                </div>
                
                <div class="nk-block-head-content">
                    <ul class="nk-block-tools g-3">
                        <li class="nk-block-tools-opt">
                            <a href="{{ route('products.index') }}" class="btn btn-primary">
                                <em class="icon ni ni-arrow-left"></em>
                                <span>Back</span>
                            </a>
                        </li>
                    </ul>
                </div><!-- .nk-block-head-content -->
            </div><!-- .nk-block-head -->
        </div><!-- .nk-block-head -->
            
        @include('partials.session')
        @include('partials.error')
        
        <div class="nk-block nk-block-lg">
            <div class="card card-bordered">
                <div class="card-inner">
                    <div class="card-head">
                        <h5 class="card-title">{{ isset($product)?'Edit':'New' }} Product Setup</h5>
                    </div>
                    <form action="{{ isset($product)? route('products.update',$product->id):route('products.store') }}" class="gy-3" class="is-alter form-validate" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @if (isset($product))
                            {{ method_field('PUT') }}
                        @endif
                        <div class="row g-4">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="form-label" for="is_headline">Brand *</label>
                                    <div class="form-control-wrap">
                                        <select name="brand_id" id="brand_id" class="form-select" >
                                            <option value="0" disabled selected>Select Brand *</option>
                                            @foreach ($brands as $brand)
                                                <option value="{{ $brand->id }}"
                                                    @if (isset($product))
                                                        @if ($brand->id==$product->brand_id)
                                                            selected
                                                        @endif
                                                    @endif
                                                    >{{ $brand->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="form-label" for="is_headline">Category *</label>
                                    <div class="form-control-wrap">
                                        <select name="category_id" id="category_id" class="form-select" >
                                            <option value="0" disabled selected>Select Category*</option>
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}"
                                                    @if (isset($product))
                                                        @if ($category->id==$product->category_id)
                                                            selected
                                                        @endif
                                                    @endif
                                                    >{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="form-label" for="is_headline">Sub Category</label>
                                    <div class="form-control-wrap">
                                        <select name="subcategory_id" id="subcategory_id" class="form-select" >
                                            <option value="0" disabled>Select Sub Category*</option>
                                            @if (isset($product))
                                                @foreach ($subcategories as $category)
                                                    <option value="{{ $category->id }}"
                                                        @if (isset($product))
                                                            @if ($category->id==$product->subcategory_id)
                                                                selected
                                                            @endif
                                                        @endif
                                                        >{{ $category->name }}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label" for="name">Product Name *</label>
                                    <div class="form-control-wrap">
                                        <input type="text" class="form-control" name="name" id="name" value="{{ isset($product)?$product->name:old('name') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label" for="description">Product Description *</label>
                                    <div class="form-control-wrap">
                                        <textarea class="form-control"  name="description" id="description" cols="30" rows="2">{{ isset($product)?$product->description:old('description')}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label" for="amount">Amount</label>
                                    <div class="form-control-wrap">
                                        <input type="text" class="form-control" name="amount" id="amount" value="{{ isset($product)?$product->amount:old('amount') }}">
                                    </div>
                                </div>
                            </div><!--
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label" for="sku">SKU</label>
                                    <div class="form-control-wrap">
                                        <input type="text" class="form-control" name="sku" id="sku" value="{{ isset($product)?$product->sku:old('sku') }}">
                                    </div>
                                </div>
                            </div>-->
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label" for="barcode">Barcode</label>
                                    <div class="form-control-wrap">
                                        <input type="text" class="form-control" name="barcode" id="barcode" value="{{ isset($product)?$product->barcode:old('barcode') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label" for="url">URL</label>
                                    <div class="form-control-wrap">
                                        <input type="text" class="form-control" name="url" id="url" value="{{ isset($product)?$product->url:old('url') }}">
                                    </div>
                                </div>
                            </div>

                            @if (isset($product))
                            <div class="form-group">
                                <img src="{{ asset('storage/'.$product->image) }}" style="width: 100%" >
                            </div>
                            @endif
                            
                    
                            <div class="col-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-lg btn-primary"><em class="icon ni ni-save"></em><span>Save & Next</span> </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- .nk-block -->
@endsection

@section('script')
    
   

<script type="text/javascript">

    
    $(document).ready(function () {
            



        $('#category_id').on('change', function () {
            let id = $(this).val();
            $('#subcategory_id').empty();
            $.ajax({
                type: 'GET',
                url: 'getSubCat/' + id,
                success: function (response) {
                    var response = JSON.parse(response); 
                    $('#subcategory_id').empty();
                    $('#subcategory_id').append(`<option value="0" disabled selected>Select Sub Category*</option>`);
                    response.forEach(element => {
                        $('#subcategory_id').append(`<option value="${element['id']}">${element['name']}</option>`);
                    });
                }
            });
        });
    });


</script>
@endsection
