@extends('layouts.site')

@section('title')
    <title>Story - Dreamron</title>
@endsection

@section('bodyclass')
    class="to-top on-side-way"
@endsection




@section('content')
        <style>
            #story{
                color: #000;
                font-weight: 500;
            }
            
            .read-more-content img{
                width: 20%;
            }
            
            h1,h4{
                color:#211651;
            }
            .slider-text-container p {
                margin-top: 0px;
                margin-bottom: 20px;
            }
            @media only screen and (max-width: 375px) {

                p.mobile { 
   font-size: 0.78rem; 
}

}
@media only screen and (max-width: 600px) 
            {


                .fixed-header{
                    position: fixed;        
                }
            }
            
            @media only screen and (max-width: 600px) 
            {


                .headercolor{
                    background-color: rgb(255, 255, 255);
                    padding-top: -10px;
                    padding-bottom: 63px;
                    z-index: 1;
                    /*margin-bottom:100px;*/
                            
                }
            }
            
            @media only screen and (max-width: 400px) 
            {


                .applecss{
                    margin-top:-400px;
                            
                }
            }
            
            @media only screen and (max-width: 400px) 
            {


                .appleourhistory2{
                    margin-bottom:200px;
                            
                }
            }
            
        </style>
        <div class="container-fluid dreamron-products">
            <div class="row headercolor">
                <!--side-panel-->
                <div class="col-lg-2 p-0 main-side-panel fixed-header">
                    @include('partials.search')
    
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <!--<span class="navbar-toggler-icon"></span>-->
                            <div id="nav-icon" class="nav-icon">
                                <div class="nav-icon-inner">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </button>
    
                        @include('partials.sidebar')
                    </nav>
                </div>
    
                <script>
                    $(document).ready(function(){
                        $('#nav-icon').click(function(){
                            $(this).toggleClass('open');
                        });
                    });
                </script>
                <!--side-panel-->
    
                <div class="col-lg-10 p-0 main-products">
                    
                    <!--header-->
                    <div class="main-header headercolor">
                        <div class="main-header-inner text-center">
                            <a href="{{ route('welcome')}}"><img src="{{ asset('site/img/dreamron.png?v=1.000.000.012') }}" alt="Dreamron"></a>
                        </div>
                    </div>
                    <!--header-->

                    <div class="unique-product-container" id="myDiv">
                        <button class="navbar-toggler" style="float: right">
                            <div id="nav-icon-prod" class="nav-icon open">
                                <div class="nav-icon-inner">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </button>
                        <div class="read-more-content h-100 ">
                            <img src="{{ asset('site/img/dreamron.png') }}" alt="read more content" >
        
                            <h3 class="readmore-head chairman">Chairman’s message</h3>
        
                            <p class="readmore-content chairman">
                                Thank you for your decision to connect with Dreamron.<br><br>
                                In keeping with the vision, mission and values of the Dreamron Group of Companies, we strive to create a better future for all Sri-Lankans with a Business Model that creates significant economic value for the country. <br><br>
                                We are pleased with the successful journey of the Dreamron Group over the past 2 decades focusing on a strategically important business portfolio that includes; Brand Marketing, Manufacturing, 3rd party contract manufacturing, OEM brand manufacturing, Exports and  Beauty Education to uplift the standards of the industry to be par with the world’s best.<br><br>
                                Using the most modern global technology and sourcing of inputs from destinations that have a reputation for best cosmetic ingredients and with the required manufacturing quality certifications, we have been able to build an admired and trusted corporate brand in the eyes of all our stakeholders. Over the years, in line with the changing consumer demands and aspirations, we have expanded the product and brand portfolio to further build on the competitive advantages while offering augmented value to the intermediaries and end users. <br><br>
                                We employee people with right technical qualifications, industry knowledge & experience  and management skills to facilitate every single business process efficiently to ensure the right business results combining all physical strengths to benefit all stakeholders of the business.<br><br>
                                As a socially responsible organization, we aim to deliver a ‘triple bottom-line’ embracing global best practices, fully complying with good governance principles to grow the business by double digit on ‘year on year’ basis to help the country’s economic growth via the growth of fully locally owned, export oriented business organizations being the only way to revive the economy to benefit all Sri-Lankans.<br><br>
                                Thank you sincerely for your trust in the Group, and it’s customer our value proposition. <br><br>
                                Yours truly,<br>
                                Dr Priyanka Perera
                            </p>

                            <h3 class="readmore-head directress">Message from the Directress</h3>
                            <p class="readmore-content directress">
                                I’m pleased to note your interest in browsing our corporate web-site and wish to acknowledge it with immense gratitude.<br><br>
                                We closely monitor the trends in the cosmetic industry across the entire world and make continuous efforts to offer an improved value proposition to the end users. With the vast experience gained over the years succeeding not only in the local but global market too, we take the lead in launching innovate products under a respected brand portfolio; Dreamron, Evon and the latest addition, Aurica skin care range fulfilling a gap in the product range to offer a total solution to our customers.<br><br>
                                We believe that the beauty industry in Sri-Lanka has not yet reached the desired level; hence our   decision to expand the beauty college network while elevating the level of qualification to meet the evolving needs to be par with the world. <br><br>
                                Being able to produce over 1000 qualified beauticians at varying levels for the betterment of the local industry is an act that we take pride in from the point of view of equipping the Sri-Lankan workforce for high value jobs. These qualifications can be marketed proudly across the world bringing in most needed foreign currency. As you are very well aware, the Female work force in our country is under-employed and we believe that this is a good industry and the best way for the female workforce to secure the right compensation. We see a good trend of more males entering the industry too which welcoming from every aspect. <br><br>
                                We thank you for your continued patronage and trust placed in the group and the beauty college in particular.<br><br>

                                Thanking you<br>

                                Mrs. Nisha Perera

                            </p>

                            
                            <h3 class="readmore-head md mb-3">Message from the MD</h3>
                            <div class="embed-responsive embed-responsive-16by9 md">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/g21VOMLKt50" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="dreamron-common d-none d-sm-block">
                        <div class="hideMe">
                            <svg id="downArrow" class="arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
                                <g stroke-linejoin="round" stroke-linecap="round">
                                    <circle r="46" cx="50" cy="50"/>
                                    <polyline points="25 40, 50 70, 75 40"></polyline>
                                </g>
                            </svg>
                            <svg id="upArrow" class="arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
                                <g stroke-linejoin="round" stroke-linecap="round">
                                    <circle r="46" cx="50" cy="50"/>
                                    <polyline points="25 60, 50 30, 75 60"></polyline>
                                </g>
                            </svg>
                            <div id="masterWrap">
                                <div id="panelWrap">
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                        <div class="tns-item slider-text-item-container">
                                            <div class="slider-text-container">
                                                <div class="text-container top-up" style="display: block">
                                                    <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Who we are?</h4></div>
                                                    
    
                                                    <p>Dreamron Group of companies is in the business of manufacturing and marketing a 
                                                        wide range of quality Professional and Herbal Cosmetics, Hair Care, Skin Care, Toiletries 
                                                        products and making inroads into Perfume market to extend its product portfolio for more aggressive growth.
                                                        <br><br>
                                                        It’s 120,000 sq. ft manufacturing facilities with Japanese technology collaboration and office complex is located in a 10-acre land, certified by world leading laboratories for<strong> GMP, ISO 9001:2015 </strong>for its manufacturing practices – one plant is dedicated to exports and has so far made footprint in more than 30 countries and the other plant caters to the domestic market demand under 3 multiple brands. The raw materials are sourced from USA, Germany, France, Italy, Canada, UK, Japan and a few other countries meeting the highest EU regulations & standards to ensure the quality of products par with the best in the world.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item dreamron-sty1">
    
                                            </div>
                                        </div>
                                    </section>
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                        <div class="tns-item slider-text-item-container">
                                            <div class="slider-text-container">
                                                <div class="text-container top-up">
                                                    <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Vision & Mission</h4></div>
                                                   <!-- <h4><span class="hover hover-1">Vision</span>
                                                    </h4> -->
                                                    <p>To be the most admired & respected local cosmetic manufacturer and marketer that fully meets the consumer’s quality demands both locally and internationally in a manner that creates real value for the country thru best of technology, processes, people and partnerships.
                                                    </p>
                                                   <!-- 
                                                    <h4>Mission
                                                    </h4> -->
                                                    <p>To deliver the expectations of the beauty conscious customers via a range of high-quality products combining multitude of strengths, gained over decades of industry experience & expertise to build a sustainable business to benefit all Sri Lankans. We strive to elevate the professionalism of the beauty industry through globally recognized qualifications.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item dreamron-sty2">
    
                                            </div>
                                        </div>
                                    </section>
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0  tns-outer dreamron-text-slider">
                                            <div class="tns-item slider-text-item-container">
                                                <div class="slider-text-container" >
                                                    <div class="text-container top-up">
                                                        {{-- <div class="bred-crumb ml-4"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Four Pillars Dreamron Group stands on</h4></div> --}}
                                                        <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Four Pillars Dreamron Group stands on</h4></div>
                                                        <!--
                                                        <h4  >Four Pillars Dreamron Group stands on
                                                        </h4>-->
    
                                                        
                                                        <h6>Trust and Candor</h6><p> Conduct ourselves professionally with candor, respect and integrity. </p>
                                                        <h6>Passion for Excellence</h6><p>  Pace-setting and innovative, always striving to find a better way. Doing it right the first time every time.</p>
                                                        <h6>Result Oriented</h6><p> ‘do what it takes’ attitude. Empowered and entrepreneurial operations within a common framework of existing culture, values, strategies and key processes.</p> 
                                                        <h6>Accountability</h6><p> Get all to believe in accepting responsibility and the consequences, and taking ownership while embracing common goals, teamwork, and collaborative decision-making.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item dreamron-sty4">
    
                                            </div>
                                        </div>
                                    </section>
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                            <div class="tns-item slider-text-item-container">
                                                <div class="slider-text-container">
                                                    <div class="text-container top-up">
                                                        <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Our History</h4></div>
                                                        
                                                        <!--<h4>Our History
                                                        </h4>-->
                                                        

                                                <div class="year-table mb-3">
                                                    <div class="year-row">
                                                        <div class="year-val">
                                                            1997
                                                        </div>
                                                        <div class="year-info">
                                                            Kindai Kagaku Lanka (Pvt) Ltd – manufacturing plant was established.
                                                        </div>
                                                    </div>
                                                    <div class="year-row">
                                                        <div class="year-val">
                                                            1998
                                                        </div>
                                                        <div class="year-info">
                                                            Kindai Kagaku Lanka (Pvt) Ltd starts operation in Sri Lanka.
                                                        </div>
                                                    </div>
                                                    <div class="year-row">
                                                        <div class="year-val">
                                                            1999
                                                        </div>
                                                        <div class="year-info">
                                                            Kindai Kagaku Lanka (Pvt.) Ltd starts exporting products to Japan.
                                                        </div>
                                                    </div>
                                                    <div class="year-row">
                                                        <div class="year-val">
                                                            1998
                                                        </div>
                                                        <div class="year-info">
                                                            Dreamron Lanka Limited starts its distribution operations in Sri Lanka.
                                                        </div>
                                                    </div>
                                                    <div class="year-row">
                                                        <div class="year-val">
                                                            1998
                                                        </div>
                                                        <div class="year-info">
                                                            Kindai Kagaku Lanka (Pvt) Ltd start sales & marketing operation in India after establishing Dreamron India.
                                                        </div>
                                                    </div>
                                                    <div class="year-row">
                                                        <div class="year-val">
                                                            2004
                                                        </div>
                                                        <div class="year-info">
                                                            Harumi Holdings (Pvt) Ltd was established and starts operations in Sri Lanka to provide quality products and world class customer service to local customers and industries.
                                                        </div>
                                                    </div>
                                                    <div class="year-row">
                                                        <div class="year-val">
                                                            2005
                                                        </div>
                                                        <div class="year-info">
                                                            Harumi Holding commences 3rd party contract manufacturing for one of the world’s largest multi-national companies. 
                                                        </div>
                                                    </div>
                                                </div>
    
                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item dreamron-sty5">
    
                                            </div>
                                        </div>
                                    </section>
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                            <div class="tns-item slider-text-item-container">
                                                <div class="slider-text-container">
                                                    <div class="text-container top-up">
                                                        <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Our History</h4></div>
                                                        
                                                        <!--<h4>
                                                        </h4>-->

                                                        
                                                <div class="year-table">
                                                    <div class="year-row">
                                                        <div class="year-val">
                                                            2005
                                                        </div>
                                                        <div class="year-info">
                                                            Kindai Kagaku Lanka (Pvt) Ltd starts sales & marketing operations in Pakistan as Dreamron Pakistan.
                                                        </div>
                                                    </div>
                                                    <div class="year-row">
                                                        <div class="year-val">
                                                            2008
                                                        </div>
                                                        <div class="year-info">
                                                            Dreamron College of Art and Beauty starts its operations.
                                                        </div>
                                                    </div>
                                                    <div class="year-row">
                                                        <div class="year-val">
                                                            2010
                                                        </div>
                                                        <div class="year-info">
                                                            EVON cosmetic Brand was introduced to the Local and international markets.
                                                        </div>
                                                    </div>
                                                    <div class="year-row">
                                                        <div class="year-val">
                                                            2015
                                                        </div>
                                                        <div class="year-info">
                                                            Dreamron Group appointed its 100th Retail distributor in Sri Lanka.
                                                        </div>
                                                    </div>
                                                    <div class="year-row">
                                                        <div class="year-val">
                                                            2016
                                                        </div>
                                                        <div class="year-info">
                                                            Kindai Kagaku Lanka (Pvt) Ltd starts sales & marketing operations in Bangladesh and Vietnam as Dreamron Bangladesh and Dreamron Vietnam.
                                                        </div>
                                                    </div>
                                                    <div class="year-row">
                                                        <div class="year-val">
                                                            2017
                                                        </div>
                                                        <div class="year-info">
                                                            Kindai Kagaku Lanka (Pvt.) Ltd enters Myanmar market as Dreamron Myanmar.
                                                        </div>
                                                    </div>
                                                    <div class="year-row">
                                                        <div class="year-val">
                                                            2018
                                                        </div>
                                                        <div class="year-info">
                                                            Kindai Kagaku Lanka (Pvt) Ltd starts sales & marketing operations in Australia as Dreamron Australia
                                                        </div>
                                                    </div>
                                                    <div class="year-row">
                                                        <div class="year-val">
                                                            2020
                                                        </div>
                                                        <div class="year-info">
                                                            ‘AURICA’ skincare brand was introduced to the local and international markets.
                                                        </div>
                                                    </div>
                                                </div>
    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item dreamron-sty6">
    
                                            </div>
                                        </div>
                                    </section>
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                            <div class="tns-item slider-text-item-container">
                                                <div class="slider-text-container">
                                                    <div class="text-container top-up">
                                                        <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Leadership</h4></div>
                                                        
                                                        <!--<h4>Leadership
                                                        </h4>-->
                                                        <h5>Chairman – Dr. Priyanka Perera</h5>
    
                                                        <p class="mobile">
                                                            The Chairman of Dreamron Group of Companies with a proven track record as an academic having ventured into business of cosmetics has created one of the largest cosmetic manufacturing and export organizations in Sri Lanka. His patriotic and visionary leadership has helped the country to generate foreign revenue thru exports into 27 countries. Competing in an industry that is dominated by multinational brands, Dr. Perera has proven the fact that Sri-Lankan enterprises in any industry can compete effectively to build a home grown economy for sustainable economic growth.   
                                                            <br>
                                                            Dreamron College of Art & Beauty, a brain child of Dr. Perera is by far the largest privately owned Beauty College in Sri-Lanka offering world recognized professional qualifications to the sri-Lankan youth not only to elevate the professionalism of the beauty industry but find high value jobs in overseas markets.
                                                            <br>
                                                            Dr. Priyanka Perera is well versed with the trends and evolution of the global cosmetic industry and keen to make timely moves to stay ahead of the curve to benefit all stakeholders of the business.
                                                            <br><button type="button" class="btn btn-outline-danger mt-1 read-more btn-internal" data-type="chairman" >Chairman’s message</button>
                                                        </p>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item dreamron-sty7">
    
                                            </div>
                                        </div>
                                    </section>
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                            <div class="tns-item slider-text-item-container">
                                                <div class="slider-text-container">
                                                    <div class="text-container top-up">
                                                        <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Leadership</h4></div>
                                                        
                                                        <!--<h4>Leadership
                                                        </h4>-->
                                                        <h4>Directress – Mrs. Nisha Perera</h4>
    
                                                        <p >
                                                            Mrs. Nisha Perera is a veteran in the field of beauty education with her vast hands on experience leading the network of beauty colleges in addition to being a member of the Board of Directors of Dreamron Group of Companies. She actively contributes towards enhancing the value of the entire group in numerous aspects.  Over the past several years the beauty college has expanded its business in leaps and bounds to benefit the industry. She has been the architect of obtaining many accreditations to enhance the value of the qualification and to be recognized globally. 
                                                            <br><button type="button" class="btn btn-outline-danger mt-1 read-more  btn-internal" data-type="directress" >Directress’s message</button>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item dreamron-sty9">
    
                                            </div>
                                        </div>
                                    </section>
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                            <div class="tns-item slider-text-item-container">
                                                <div class="slider-text-container">
                                                    <div class="text-container top-up">
                                                        <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Leadership</h4></div>
                                                        
                                                        <!--<h4>Leadership
                                                        </h4>-->
                                                         <h4>Group CEO / Managing Director – Dr. Kishu Gomes</h4>
                                                        
                                                        <p class="mobile">
                                                            Dr. Kishu Gomes is an award-winning Corporate Leader and a Marketing professional with over 3 decades of work experience with US multinationals in two huge global industries namely Beverage and Energy. He has gained foreign exposure through country assignments, global & regional project work and leadership roles and got a proven track record for corporate management, business re-engineering, strategic marketing, procurement, manufacturing, project management, export market entry and modern-day HR philosophy & practice - all of which to improve the competitiveness of an organization being the cornerstones to creating and sustaining superior business performance. Before joining the Dreamron Group he was the Chairman of Sri-Lanka Tourism. Dr Gomes has created many records along his corporate journey that provides ample inspiration to the younger generation to challenge the world.
                                                            <br><button type="button" class="btn btn-outline-danger mt-1 read-more  btn-internal" data-type="md" >MD’s message</button>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item dreamron-sty8">
    
                                            </div>
                                        </div>
                                    </section>
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                            <div class="tns-item slider-text-item-container">
                                                <div class="slider-text-container">
                                                    <div class="text-container top-up">
                                                        <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Leadership</h4></div>
                                                        
                                                        <!--<h4>Leadership
                                                        </h4>-->
                                                        <h4>Director Finance – Mr. Senarath Devendra</h4>
    
                                                        <p >
                                                            Mr. Devendra is a Fellow member of the Institute of Chartered Accountants of Sri Lanka and Fellow Management Accountant. Multidiscipline Professional in Finance, Marketing & IT. He joined Dreamron group in 2017 as the Group Finance Director and was invited to the Board. Before joining Dreamron he was the Managing Director / Chief Executive Officer of Listed Plantation Company in the Colombo Stock Exchange Managing over 12,000 HA of Plantations with approx. 10,000 workforce. In addition to the Plantations, he headed the Leisure Sector of a leading group of companies in Sri Lanka. 
                                                            His Career of over 35 Years consisting with many Multi National FMCG and International Hospitality Industry Giants and the largest local conglomerate are few of his milestones.
                                                            
                                                           </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item dreamron-sty10">
    
                                            </div>
                                        </div>
                                    </section>
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                            <div class="tns-item slider-text-item-container">
                                                <div class="slider-text-container">
                                                    <div class="text-container top-up">
                                                        <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Our Group</h4></div>
                                                        
                                                        <!--<h4>Our Group
                                                        </h4>-->
                                                        <h4>Kindai Kagaku Lanka (Pvt) Ltd</h4>
    
                                                        <p >
                                                            Kindai Kagku Lanka (PVT) Limited is a pioneering cosmetic manufacturing company with Japanese technology that’s approved by the Board of Investment for exports. Currently Dreamron products are marketed in over 30 countries including Japan, Canada, Netherlands, Sweden, Australia, Poland, Norway, India, Bangladesh and Pakistan and efforts are underway to enter several new markets across the globe. Kindai Kagaku Lanka also undertakes private label manufacturing for several leading global cosmetic and toiletries brands. 
                                                            <br>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item dreamron-sty11">
    
                                            </div>
                                        </div>
                                    </section>
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                            <div class="tns-item slider-text-item-container">
                                                <div class="slider-text-container">
                                                    <div class="text-container top-up">
                                                        <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Our Group</h4></div>
                                                        
                                                        <!--<h4>Our Group
                                                        </h4>-->
                                                         <h4>Harumi Holdings (Pvt) Ltd</h4>
    
                                                        <p >

                                                            Harumi Holdings is the manufacturing arm of Dreamron Group which does manufacturing of Dreamron, Evon and Aurica branded product for domestic the market.  Primary business of Harumi is 3rd party contract manufacturing and currently manufactures products for a few multinationals and a local conglomerate. Harumi Holdings is also equipped with a Pre-formed plastic bottling plant making it a vertically integrated supply chain model for business growth thru flexibility.
                                                            
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item dreamron-sty12">
    
                                            </div>
                                        </div>
                                    </section>
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                            <div class="tns-item slider-text-item-container">
                                                <div class="slider-text-container">
                                                    <div class="text-container top-up">
                                                        <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Our Group</h4></div>
                                                        
                                                        <!--<h4>Our Group
                                                        </h4>-->
                                                         <h4>Dreamron Lanka (Pvt) Ltd</h4>
    
                                                        <p >

                                                            Dreamron Lanka (Pvt.) Limited was established in the year 2002. Dreamron Lanka at present is one of the largest cosmetic product distribution and trading companies with close to 100 distributors across the island manned by over two hundred professional Sales, Distribution, Marketing staff and administrative staff. Having its Central Warehouse with over three hundred SKUs on the outskirts of capital city allows Dreamron Lanka Limited to carry out its distribution operation seamlessly to provide its customers an uninterrupted service.
                                                            <br><br>
                                                            As the brand custodian of “Dreamron”, “Aurica”, Kleara, and Evon, Dreamron Lanka limited, continues its strong market position as the leader in the Sri Lankan professional market segment.


                                                            <br><a href="https://dreamron.lk/" target="_blank" type="button" class="btn btn-outline-danger mt-2  btn-internal" >View More</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item dreamron-sty13">
    
                                            </div>
                                        </div>
                                    </section>
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                            <div class="tns-item slider-text-item-container">
                                                <div class="slider-text-container">
                                                    <div class="text-container top-up">
                                                        <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Our Group</h4></div>
                                                        
                                                        <!--<h4>Our Group
                                                        </h4>-->
                                                         <h4>Dreamron College of Art and Beauty</h4>
    
                                                        <p >
                                                            Dreamron Beauty College being one of the Corporate Social Responsibility Projects undertaken by the Dreamron Group has 8 branches covering all key cities in the island and over 1000 professional beauty diploma holders pass out annually. Apart from its own diploma courses which are recognized internationally, Dreamron College also conducts programs offered by City and Guilds in United Kingdom.This initiative helps empower professionals in the industry to elevate their professionalism for quality delivery par with the best in the world.                                                            
                                                            
                                                            <br><a href="http://www.dreamronbeautycollege.com/" target="_blank" type="button" class="btn btn-outline-danger mt-2  btn-internal" >View More</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item dreamron-sty14">
    
                                            </div>
                                        </div>
                                    </section>
    
                                </div>
                            </div>
                            <div class="dots">
                            </div>
                            <div class="toolTips">
                                <div class="toolTip">
                                    Who we are?
                                </div>
                                <div class="toolTip">
                                    Vision & Mission
                                </div>
                                <div class="toolTip">
                                    Four Pillars Dreamron Group stands on
                                </div>
                                <div class="toolTip">
                                    Our History
                                </div>
                                <div class="toolTip">
                                    Our History
                                </div>
                                <div class="toolTip">
                                    Chairman – Dr. Priyanka Perera
                                </div>
                                <div class="toolTip">
                                    Directress – Mrs. Nisha Perera
                                </div>
                                <div class="toolTip">
                                    Group CEO / Managing Director – Dr. Kishu Gomes
                                </div>
                                <div class="toolTip">
                                    Director Finance – Mr. Senarath Devendra
                                </div>
                                <div class="toolTip">
                                    Kindai Kagaku Lanka (Pvt) Ltd
                                </div>
                                <div class="toolTip">
                                    Harumi Holdings (Pvt) Ltd
                                </div>
                                <div class="toolTip">
                                    Dreamron Lanka (Pvt) Ltd
                                </div>
                                <div class="toolTip">
                                    Dreamron College of Art and Beauty
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-block d-sm-none applecss">
                        <section class="row m-0 dreamron-item dreamron-sty1" >
                            <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.8;">
                            <div class="tns-item slider-text-item-container">
                                <div class="slider-text-container">
                                    <div class="text-container top-up" >
                                        <div class="bred-crumb" ><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Who we are?</h4></div>
                                        
                                        
                                        <p>Dreamron Group of companies is in the business of manufacturing and marketing a 
                                            wide range of quality Professional and Herbal Cosmetics, Hair Care, Skin Care, Toiletries 
                                            products and making inroads into Perfume market to extend its product portfolio for more aggressive growth.
                                            <br><br>
                                            It’s 120,000 sq. ft manufacturing facilities with Japanese technology collaboration and office complex is located in a 10-acre land, certified by world leading laboratories for<strong> GMP, ISO 9001:2015 </strong>for its manufacturing practices – one plant is dedicated to exports and has so far made footprint in more than 30 countries and the other plant caters to the domestic market demand under 3 multiple brands. The raw materials are sourced from USA, Germany, France, Italy, Canada, UK, Japan and a few other countries meeting the highest EU regulations & standards to ensure the quality of products par with the best in the world.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </section>
                        <section class="row m-0 dreamron-item dreamron-sty2" style="overflow:scroll">
                            <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.8">
                            <div class="tns-item slider-text-item-container">
                                <div class="slider-text-container">
                                    <div class="text-container top-up">
                                        <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Vision & Mission</h4></div>
                                       <!-- <h4><span class="hover hover-1">Vision</span>
                                        </h4> -->
                                        <p>To be the most admired & respected local cosmetic manufacturer and marketer that fully meets the consumer’s quality demands both locally and internationally in a manner that creates real value for the country thru best of technology, processes, people and partnerships.
                                        </p>
                                       <!-- 
                                        <h4>Mission
                                        </h4> -->
                                        <p>To deliver the expectations of the beauty conscious customers via a range of high-quality products combining multitude of strengths, gained over decades of industry experience & expertise to build a sustainable business to benefit all Sri Lankans. We strive to elevate the professionalism of the beauty industry through globally recognized qualifications.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </section>
                        <section class="row m-0 dreamron-item dreamron-sty4">
                            <div class="col-md-6 col-lg-5 p-0  tns-outer" style="background-color: aliceblue; opacity:0.8">
                                <div class="tns-item slider-text-item-container">
                                    <div class="slider-text-container" >
                                        <div class="text-container top-up">
                                            {{-- <div class="bred-crumb ml-4"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Four Pillars Dreamron Group stands on</h4></div> --}}
                                            <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Four Pillars Dreamron Group stands on</h4></div>
                                            <!--
                                            <h4  >Four Pillars Dreamron Group stands on
                                            </h4>-->

                                            
                                            <h6>Trust and Candor</h6><p> Conduct ourselves professionally with candor, respect and integrity. </p>
                                            <h6>Passion for Excellence</h6><p>  Pace-setting and innovative, always striving to find a better way. Doing it right the first time every time.</p>
                                            <h6>Result Oriented</h6><p> ‘do what it takes’ attitude. Empowered and entrepreneurial operations within a common framework of existing culture, values, strategies and key processes.</p> 
                                            <h6>Accountability</h6><p> Get all to believe in accepting responsibility and the consequences, and taking ownership while embracing common goals, teamwork, and collaborative decision-making.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="row m-0 dreamron-item dreamron-sty5 appleourhistory2">
                            <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.8">
                                <div class="tns-item slider-text-item-container">
                                    <div class="slider-text-container">
                                        <div class="text-container top-up">
                                            <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Our History</h4></div>
                                            
                                            <!--<h4>Our History
                                            </h4>-->
                                            

                                    <div class="year-table mb-3">
                                        <div class="year-row">
                                            <div class="year-val">
                                                1997
                                            </div>
                                            <div class="year-info">
                                                Kindai Kagaku Lanka (Pvt) Ltd – manufacturing plant was established.
                                            </div>
                                        </div>
                                        <div class="year-row">
                                            <div class="year-val">
                                                1998
                                            </div>
                                            <div class="year-info">
                                                Kindai Kagaku Lanka (Pvt) Ltd starts operation in Sri Lanka.
                                            </div>
                                        </div>
                                        <div class="year-row">
                                            <div class="year-val">
                                                1999
                                            </div>
                                            <div class="year-info">
                                                Kindai Kagaku Lanka (Pvt.) Ltd starts exporting products to Japan.
                                            </div>
                                        </div>
                                        <div class="year-row">
                                            <div class="year-val">
                                                1998
                                            </div>
                                            <div class="year-info">
                                                Dreamron Lanka Limited starts its distribution operations in Sri Lanka.
                                            </div>
                                        </div>
                                        <div class="year-row">
                                            <div class="year-val">
                                                1998
                                            </div>
                                            <div class="year-info">
                                                Kindai Kagaku Lanka (Pvt) Ltd start sales & marketing operation in India after establishing Dreamron India.
                                            </div>
                                        </div>
                                    </div>

        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="row m-0 dreamron-item dreamron-sty6">
                            <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.8">
                                <div class="tns-item slider-text-item-container">
                                    <div class="slider-text-container">
                                        <div class="text-container top-up">
                                            <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Our History</h4></div>
                                            
                                            <!--<h4>
                                            </h4>-->

                                            
                                    <div class="year-table">
                                        <div class="year-row">
                                            <div class="year-val">
                                                2004
                                            </div>
                                            <div class="year-info">
                                                Harumi Holdings (Pvt) Ltd was established and starts operations in Sri Lanka to provide quality products and world class customer service to local customers and industries.
                                            </div>
                                        </div>
                                        <div class="year-row">
                                            <div class="year-val">
                                                2005
                                            </div>
                                            <div class="year-info">
                                                Harumi Holding commences 3rd party contract manufacturing for one of the world’s largest multi-national companies. 
                                            </div>
                                        </div>
                                        <div class="year-row">
                                            <div class="year-val">
                                                2005
                                            </div>
                                            <div class="year-info">
                                                Kindai Kagaku Lanka (Pvt) Ltd starts sales & marketing operations in Pakistan as Dreamron Pakistan.
                                            </div>
                                        </div>
                                        <div class="year-row">
                                            <div class="year-val">
                                                2008
                                            </div>
                                            <div class="year-info">
                                                Dreamron College of Art and Beauty starts its operations.
                                            </div>
                                        </div>
                                        <div class="year-row">
                                            <div class="year-val">
                                                2010
                                            </div>
                                            <div class="year-info">
                                                EVON cosmetic Brand was introduced to the Local and international markets.
                                            </div>
                                        </div>
                                    </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="row m-0 dreamron-item dreamron-sty5 appleourhistory2">
                            <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.8">
                                <div class="tns-item slider-text-item-container">
                                    <div class="slider-text-container">
                                        <div class="text-container top-up">
                                            <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Our History</h4></div>
                                            
                                            <!--<h4>Our History
                                            </h4>-->
                                            

                                    <div class="year-table mb-3">
                                        <div class="year-row">
                                            <div class="year-val">
                                                2015
                                            </div>
                                            <div class="year-info">
                                                Dreamron Group appointed its 100th Retail distributor in Sri Lanka.
                                            </div>
                                        </div>
                                        <div class="year-row">
                                            <div class="year-val">
                                                2016
                                            </div>
                                            <div class="year-info">
                                                Kindai Kagaku Lanka (Pvt) Ltd starts sales & marketing operations in Bangladesh and Vietnam as Dreamron Bangladesh and Dreamron Vietnam.
                                            </div>
                                        </div>
                                        <div class="year-row">
                                            <div class="year-val">
                                                2017
                                            </div>
                                            <div class="year-info">
                                                Kindai Kagaku Lanka (Pvt.) Ltd enters Myanmar market as Dreamron Myanmar.
                                            </div>
                                        </div>
                                        <div class="year-row">
                                            <div class="year-val">
                                                2018
                                            </div>
                                            <div class="year-info">
                                                Kindai Kagaku Lanka (Pvt) Ltd starts sales & marketing operations in Australia as Dreamron Australia
                                            </div>
                                        </div>
                                        <div class="year-row">
                                            <div class="year-val">
                                                2020
                                            </div>
                                            <div class="year-info">
                                                ‘AURICA’ skincare brand was introduced to the local and international markets.
                                            </div>
                                        </div>
                                    </div>

        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="row m-0 dreamron-item dreamron-sty7">
                            <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.8;">
                                <div class="tns-item slider-text-item-container">
                                    <div class="slider-text-container">
                                        <div class="text-container top-up">
                                            <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Leadership</h4></div>
                                            
                                            <!--<h4>Leadership
                                            </h4>-->
                                            <h5>Chairman – Dr. Priyanka Perera</h5>

                                            <p class="mobile">
                                                The Chairman of Dreamron Group of Companies with a proven track record as an academic having ventured into business of cosmetics has created one of the largest cosmetic manufacturing and export organizations in Sri Lanka. His patriotic and visionary leadership has helped the country to generate foreign revenue thru exports into 27 countries. Competing in an industry that is dominated by multinational brands, Dr. Perera has proven the fact that Sri-Lankan enterprises in any industry can compete effectively to build a home grown economy for sustainable economic growth.   
                                                <br>
                                                Dreamron College of Art & Beauty, a brain child of Dr. Perera is by far the largest privately owned Beauty College in Sri-Lanka offering world recognized professional qualifications to the sri-Lankan youth not only to elevate the professionalism of the beauty industry but find high value jobs in overseas markets.
                                                <br>
                                                Dr. Priyanka Perera is well versed with the trends and evolution of the global cosmetic industry and keen to make timely moves to stay ahead of the curve to benefit all stakeholders of the business.
                                                {{-- <br><button type="button" class="btn btn-outline-danger mt-1 read-more btn-internal" data-type="chairman" id="chairmanbutton">Chairman’s message</button> --}}
                                                <button type="button" class="btn btn-outline-danger mt-1" data-toggle="modal" data-target="#exampleModal">
                                                    Chairman’s message
                                                  </button>
                                            </p>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="row m-0 dreamron-item dreamron-sty9">
                            <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.8">
                                <div class="tns-item slider-text-item-container">
                                    <div class="slider-text-container">
                                        <div class="text-container top-up">
                                            <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Leadership</h4></div>
                                            
                                            <!--<h4>Leadership
                                            </h4>-->
                                            <h4>Directress – Mrs. Nisha Perera</h4>

                                            <p >
                                                Mrs. Nisha Perera is a veteran in the field of beauty education with her vast hands on experience leading the network of beauty colleges in addition to being a member of the Board of Directors of Dreamron Group of Companies. She actively contributes towards enhancing the value of the entire group in numerous aspects.  Over the past several years the beauty college has expanded its business in leaps and bounds to benefit the industry. She has been the architect of obtaining many accreditations to enhance the value of the qualification and to be recognized globally. 
                                                {{-- <br><button type="button" class="btn btn-outline-danger mt-1 read-more  btn-internal" data-type="directress" id="directressbutton">Directress’s message</button> --}}
                                                <br><button type="button" class="btn btn-outline-danger mt-1" data-toggle="modal" data-target="#exampleModal2">
                                                    Directress’s message
                                                  </button>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="row m-0 dreamron-item dreamron-sty8">
                            <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.8">
                                <div class="tns-item slider-text-item-container">
                                    <div class="slider-text-container">
                                        <div class="text-container top-up">
                                            <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Leadership</h4></div>
                                            
                                            <!--<h4>Leadership
                                            </h4>-->
                                             <h4>Group CEO / Managing Director – Dr. Kishu Gomes</h4>
                                            
                                            <p class="mobile">
                                                Dr. Kishu Gomes is an award-winning Corporate Leader and a Marketing professional with over 3 decades of work experience with US multinationals in two huge global industries namely Beverage and Energy. He has gained foreign exposure through country assignments, global & regional project work and leadership roles and got a proven track record for corporate management, business re-engineering, strategic marketing, procurement, manufacturing, project management, export market entry and modern-day HR philosophy & practice - all of which to improve the competitiveness of an organization being the cornerstones to creating and sustaining superior business performance. Before joining the Dreamron Group he was the Chairman of Sri-Lanka Tourism. Dr Gomes has created many records along his corporate journey that provides ample inspiration to the younger generation to challenge the world.
                                                {{-- <br><button type="button" class="btn btn-outline-danger mt-1 read-more  btn-internal" data-type="md"  id="mdbutton">MD’s message</button> --}}
                                                <br><button type="button" class="btn btn-outline-danger mt-1" data-toggle="modal" data-target="#exampleModal3">
                                                    MD’s message
                                                  </button>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section class="row m-0 dreamron-item dreamron-sty10">
                            <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.8">
                                <div class="tns-item slider-text-item-container">
                                    <div class="slider-text-container">
                                        <div class="text-container top-up">
                                            <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Leadership</h4></div>
                                            
                                            <!--<h4>Leadership
                                            </h4>-->
                                            <h4>Director Finance – Mr. Senarath Devendra</h4>

                                            <p >
                                                Mr. Devendra is a Fellow member of the Institute of Chartered Accountants of Sri Lanka and Fellow Management Accountant. Multidiscipline Professional in Finance, Marketing & IT. He joined Dreamron group in 2017 as the Group Finance Director and was invited to the Board. Before joining Dreamron he was the Managing Director / Chief Executive Officer of Listed Plantation Company in the Colombo Stock Exchange Managing over 12,000 HA of Plantations with approx. 10,000 workforce. In addition to the Plantations, he headed the Leisure Sector of a leading group of companies in Sri Lanka. 
                                                His Career of over 35 Years consisting with many Multi National FMCG and International Hospitality Industry Giants and the largest local conglomerate are few of his milestones.
                                                
                                               </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section class="row m-0 dreamron-item dreamron-sty11">
                            <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.8">
                                <div class="tns-item slider-text-item-container">
                                    <div class="slider-text-container">
                                        <div class="text-container top-up">
                                            <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Our Group</h4></div>
                                            
                                            <!--<h4>Our Group
                                            </h4>-->
                                            <h4>Kindai Kagaku Lanka (Pvt) Ltd</h4>

                                            <p >
                                                Kindai Kagku Lanka (PVT) Limited is a pioneering cosmetic manufacturing company with Japanese technology that’s approved by the Board of Investment for exports. Currently Dreamron products are marketed in over 30 countries including Japan, Canada, Netherlands, Sweden, Australia, Poland, Norway, India, Bangladesh and Pakistan and efforts are underway to enter several new markets across the globe. Kindai Kagaku Lanka also undertakes private label manufacturing for several leading global cosmetic and toiletries brands. 
                                                <br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="row m-0 dreamron-item dreamron-sty12">
                            <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.8">
                                <div class="tns-item slider-text-item-container">
                                    <div class="slider-text-container">
                                        <div class="text-container top-up">
                                            <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Our Group</h4></div>
                                            
                                            <!--<h4>Our Group
                                            </h4>-->
                                             <h4>Harumi Holdings (Pvt) Ltd</h4>

                                            <p >

                                                Harumi Holdings is the manufacturing arm of Dreamron Group which does manufacturing of Dreamron, Evon and Aurica branded product for domestic the market.  Primary business of Harumi is 3rd party contract manufacturing and currently manufactures products for a few multinationals and a local conglomerate. Harumi Holdings is also equipped with a Pre-formed plastic bottling plant making it a vertically integrated supply chain model for business growth thru flexibility.
                                                
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="row m-0 dreamron-item dreamron-sty13">
                            <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.8">
                                <div class="tns-item slider-text-item-container">
                                    <div class="slider-text-container">
                                        <div class="text-container top-up">
                                            <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Our Group</h4></div>
                                            
                                            <!--<h4>Our Group
                                            </h4>-->
                                             <h4>Dreamron Lanka (Pvt) Ltd</h4>

                                            <p >

                                                Dreamron Lanka (Pvt.) Limited was established in the year 2002. Dreamron Lanka at present is one of the largest cosmetic product distribution and trading companies with close to 100 distributors across the island manned by over two hundred professional Sales, Distribution, Marketing staff and administrative staff. Having its Central Warehouse with over three hundred SKUs on the outskirts of capital city allows Dreamron Lanka Limited to carry out its distribution operation seamlessly to provide its customers an uninterrupted service.
                                                <br><br>
                                                As the brand custodian of “Dreamron”, “Aurica”, Kleara, and Evon, Dreamron Lanka limited, continues its strong market position as the leader in the Sri Lankan professional market segment.


                                                <br><a href="https://dreamron.lk/" target="_blank" type="button" class="btn btn-outline-danger mt-2  btn-internal" >View More</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="row m-0 dreamron-item dreamron-sty14">
                            <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider" style="background-color: aliceblue; opacity:0.8">
                                <div class="tns-item slider-text-item-container">
                                    <div class="slider-text-container">
                                        <div class="text-container top-up">
                                            <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <h4>Our Group</h4></div>
                                            
                                            <!--<h4>Our Group
                                            </h4>-->
                                             <h4>Dreamron College of Art and Beauty</h4>

                                            <p >
                                                Dreamron Beauty College being one of the Corporate Social Responsibility Projects undertaken by the Dreamron Group has 8 branches covering all key cities in the island and over 1000 professional beauty diploma holders pass out annually. Apart from its own diploma courses which are recognized internationally, Dreamron College also conducts programs offered by City and Guilds in United Kingdom.This initiative helps empower professionals in the industry to elevate their professionalism for quality delivery par with the best in the world.                                                            
                                                
                                                <br><a href="http://www.dreamronbeautycollege.com/" target="_blank" type="button" class="btn btn-outline-danger mt-2  btn-internal" >View More</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

        {{-- chairmen modoe start --}}
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="row col-12 mt-3">
                <div class="col-10">
                  <h3 class="modal-title" id="exampleModalLabel">Chairman’s message</h3>
                </div>
                <div class="col-2">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 40px;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            </div>
                <div class="modal-body">
                    Thank you for your decision to connect with Dreamron.<br><br>
                    In keeping with the vision, mission and values of the Dreamron Group of Companies, we strive to create a better future for all Sri-Lankans with a Business Model that creates significant economic value for the country. <br><br>
                    We are pleased with the successful journey of the Dreamron Group over the past 2 decades focusing on a strategically important business portfolio that includes; Brand Marketing, Manufacturing, 3rd party contract manufacturing, OEM brand manufacturing, Exports and  Beauty Education to uplift the standards of the industry to be par with the world’s best.<br><br>
                    Using the most modern global technology and sourcing of inputs from destinations that have a reputation for best cosmetic ingredients and with the required manufacturing quality certifications, we have been able to build an admired and trusted corporate brand in the eyes of all our stakeholders. Over the years, in line with the changing consumer demands and aspirations, we have expanded the product and brand portfolio to further build on the competitive advantages while offering augmented value to the intermediaries and end users. <br><br>
                    We employee people with right technical qualifications, industry knowledge & experience  and management skills to facilitate every single business process efficiently to ensure the right business results combining all physical strengths to benefit all stakeholders of the business.<br><br>
                    As a socially responsible organization, we aim to deliver a ‘triple bottom-line’ embracing global best practices, fully complying with good governance principles to grow the business by double digit on ‘year on year’ basis to help the country’s economic growth via the growth of fully locally owned, export oriented business organizations being the only way to revive the economy to benefit all Sri-Lankans.<br><br>
                    Thank you sincerely for your trust in the Group, and it’s customer our value proposition. <br><br>
                    Yours truly,<br>
                    Dr Priyanka Perera
                </div>
                
              </div>
            </div>
          </div>
        {{-- chairmen model end --}}

        {{-- chairlady modoe start --}}
        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="row col-12 mt-3">
                    <div class="col-10">
                      <h3 class="modal-title" id="exampleModalLabel">Message from the Directress</h3>
                    </div>
                    <div class="col-2">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 40px;">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                </div>
                <div class="modal-body">
                    
                            <p class="readmore-content directress">
                                I’m pleased to note your interest in browsing our corporate web-site and wish to acknowledge it with immense gratitude.<br><br>
                                We closely monitor the trends in the cosmetic industry across the entire world and make continuous efforts to offer an improved value proposition to the end users. With the vast experience gained over the years succeeding not only in the local but global market too, we take the lead in launching innovate products under a respected brand portfolio; Dreamron, Evon and the latest addition, Aurica skin care range fulfilling a gap in the product range to offer a total solution to our customers.<br><br>
                                We believe that the beauty industry in Sri-Lanka has not yet reached the desired level; hence our   decision to expand the beauty college network while elevating the level of qualification to meet the evolving needs to be par with the world. <br><br>
                                Being able to produce over 1000 qualified beauticians at varying levels for the betterment of the local industry is an act that we take pride in from the point of view of equipping the Sri-Lankan workforce for high value jobs. These qualifications can be marketed proudly across the world bringing in most needed foreign currency. As you are very well aware, the Female work force in our country is under-employed and we believe that this is a good industry and the best way for the female workforce to secure the right compensation. We see a good trend of more males entering the industry too which welcoming from every aspect. <br><br>
                                We thank you for your continued patronage and trust placed in the group and the beauty college in particular.<br><br>

                                Thanking you<br>

                                Mrs. Nisha Perera

                            </p>
                </div>
                
              </div>
            </div>
          </div>
        {{-- chairlady model end --}}

        {{-- md modoe start --}}
        <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="row col-12 mt-3">
                    <div class="col-10">
                      <h3 class="modal-title" id="exampleModalLabel">Message from the MD</h3>
                    </div>
                    <div class="col-2">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 40px;">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                </div>
                <div class="modal-body">
                    
                    
                    <div class="embed-responsive embed-responsive-16by9 md">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/g21VOMLKt50" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                
              </div>
            </div>
          </div>
        {{-- md model end --}}
    
        @include('partials.footer')
@endsection

@section('script')
    <script src="{{ asset('site/js/stopexecutionontimeout.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/gsap-latest-beta.min.js?v=1.000.000.012') }}"></script>
    {{-- <script src="{{ asset('site/js/draggable3.min.js?v=1.000.000.012') }}"></script> --}}
    {{-- <script src="{{ asset('site/js/inta.mini.js?v=1.000.000.012') }}"></script> --}}
    <script src="{{ asset('site/js/scroll-slider.js?v=1.000.000.012') }}"></script>

    <!--/.foot-->
<script>
    $(document).ready(function () {
        $('.read-more').click(function () {
            var type=this.getAttribute('data-type');
            if(type=='chairman'){
                $('.md').hide();
                $('.directress').hide();
                $('.chairman').show();
            }
            if(type=='directress'){
                $('.md').hide();
                $('.chairman').hide();
                $('.directress').show();
            }
            if(type=='md'){
                $('.directress').hide();
                $('.chairman').hide();
                $('.md').show();
            }
            $(".unique-product-container").addClass("open-panel");
        });

        $('#nav-icon-prod').click(function () {
            $(".unique-product-container").removeClass("open-panel");
        });
        
        $("#chairmanbutton").click(function() {
    $('html, body').animate({
        scrollTop: $("#myDiv").offset().top
    }, 2000);
});

$("#directressbutton").click(function() {
    $('html, body').animate({
        scrollTop: $("#myDiv").offset().top
    }, 2000);
});

$("#mdbutton").click(function() {
    $('html, body').animate({
        scrollTop: $("#myDiv").offset().top
    }, 2000);
});
    });
</script>
@endsection




