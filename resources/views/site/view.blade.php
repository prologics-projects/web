@extends('layouts.site')

@section('title')
    <title>Post - Dreamron</title>
@endsection

@section('bodyclass')
    class="to-top on-side-way"
@endsection




@section('content')
        <style>
            #story{
                color: #000;
                font-weight: 500;
            }
            
            .read-more-content img{
                width: 20%;
            }
            
            h1,h4{
                color:#211651;
            }
            .slider-text-container p {
                margin-top: 0px;
                margin-bottom: 20px;
            }
            @media only screen and (max-width: 375px) {

                p.mobile { 
   font-size: 0.78rem; 
}

}
@media only screen and (max-width: 600px) 
            {


                .fixed-header{
                    position: fixed;        
                }
            }
            
            @media only screen and (max-width: 600px) 
            {


                .headercolor{
                    background-color: rgb(255, 255, 255);
                    padding-top: -10px;
                    padding-bottom: 63px;
                    z-index: 1;
                    /*margin-bottom:100px;*/
                            
                }
            }
            
            @media only screen and (max-width: 400px) 
            {


                .applecss{
                    margin-top:-400px;
                            
                }
            }
            
            @media only screen and (max-width: 400px) 
            {


                .appleourhistory2{
                    margin-bottom:200px;
                            
                }
            }
            
        </style>
        <div class="container-fluid dreamron-products">
            <div class="row headercolor">
                <!--side-panel-->
                <div class="col-lg-2 p-0 main-side-panel fixed-header">
                    @include('partials.search')
    
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <!--<span class="navbar-toggler-icon"></span>-->
                            <div id="nav-icon" class="nav-icon">
                                <div class="nav-icon-inner">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </button>
    
                        @include('partials.sidebar')
                    </nav>
                </div>
    
                <script>
                    $(document).ready(function(){
                        $('#nav-icon').click(function(){
                            $(this).toggleClass('open');
                        });
                    });
                </script>
                <!--side-panel-->
    
                <div class="col-lg-10 p-0 main-products">
                    <div class="unique-product-container" id="myDiv">
                        <button class="navbar-toggler" style="float: right">
                            <div id="nav-icon-prod" class="nav-icon open">
                                <div class="nav-icon-inner">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </button>
                    </div>
                    <div class="dreamron-common d-none d-sm-block">
                        <div class="hideMe">
                            <svg id="downArrow" class="arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
                                <g stroke-linejoin="round" stroke-linecap="round">
                                    <circle r="46" cx="50" cy="50"/>
                                    <polyline points="25 40, 50 70, 75 40"></polyline>
                                </g>
                            </svg>
                            <svg id="upArrow" class="arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
                                <g stroke-linejoin="round" stroke-linecap="round">
                                    <circle r="46" cx="50" cy="50"/>
                                    <polyline points="25 60, 50 30, 75 60"></polyline>
                                </g>
                            </svg>
                            <div id="masterWrap">
                                <div id="panelWrap">
                                    @foreach($blogs as $blog)
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                        <div class="tns-item slider-text-item-container">
                                            <div class="slider-text-container">
                                                <div class="text-container top-up" style="display: block">
                                                    <div class="bred-crumb"><h4>{{($blog->title) }}</h4></div>
                                                    <p>
                                                        {{($blog->description) }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item">
                                                <img src="{{ asset('storage/'.$blog->image) }}" style="height:100%;" alt="">
                                            </div>
                                        </div>
                                    </section>
                                    @endforeach
                                </div>
                            </div>
                            <div class="dots">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        @include('partials.footer')
@endsection

@section('script')
    <script src="{{ asset('site/js/stopexecutionontimeout.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/gsap-latest-beta.min.js?v=1.000.000.012') }}"></script>
    {{-- <script src="{{ asset('site/js/draggable3.min.js?v=1.000.000.012') }}"></script> --}}
    {{-- <script src="{{ asset('site/js/inta.mini.js?v=1.000.000.012') }}"></script> --}}
    <script src="{{ asset('site/js/scroll-slider.js?v=1.000.000.012') }}"></script>

    <!--/.foot-->
<script>
    $(document).ready(function () {
        $('.read-more').click(function () {
            var type=this.getAttribute('data-type');
            if(type=='chairman'){
                $('.md').hide();
                $('.directress').hide();
                $('.chairman').show();
            }
            if(type=='directress'){
                $('.md').hide();
                $('.chairman').hide();
                $('.directress').show();
            }
            if(type=='md'){
                $('.directress').hide();
                $('.chairman').hide();
                $('.md').show();
            }
            $(".unique-product-container").addClass("open-panel");
        });

        $('#nav-icon-prod').click(function () {
            $(".unique-product-container").removeClass("open-panel");
        });
        
        $("#chairmanbutton").click(function() {
    $('html, body').animate({
        scrollTop: $("#myDiv").offset().top
    }, 2000);
});

$("#directressbutton").click(function() {
    $('html, body').animate({
        scrollTop: $("#myDiv").offset().top
    }, 2000);
});

$("#mdbutton").click(function() {
    $('html, body').animate({
        scrollTop: $("#myDiv").offset().top
    }, 2000);
});
    });
</script>
@endsection




