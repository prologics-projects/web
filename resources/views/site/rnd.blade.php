@extends('layouts.site')

@section('title')
    <title>R & D - Dreamron</title>
@endsection



@section('bodyclass')
    class="to-top on-side-way"
@endsection

@section('content')
        
    <style>
        #randd{
            color: #000;
            font-weight: 500;
        }
        
         
        h1,h4{
            color:#211651;
        }   
        @media only screen and (max-width: 600px) 
            {


                .fixed-header{
                    position: fixed;        
                }
            }
            
            @media only screen and (max-width: 600px) 
            {


                .headercolor{
                    background-color: rgb(255, 255, 255);
                    padding-top: -10px;
                    padding-bottom: 63px;
                    z-index: 1;
                            
                }
            }
    </style>
    <div class="container-fluid randd">
        <div class="row headercolor">
            <!--side-panel-->
            <div class="col-lg-2 p-0 main-side-panel fixed-header">
                @include('partials.search')

                <nav class="navbar navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <!--<span class="navbar-toggler-icon"></span>-->
                        <div id="nav-icon" class="nav-icon">
                            <div class="nav-icon-inner">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </button>

                    @include('partials.sidebar')
                </nav>
            </div>

            <script>
                $(document).ready(function(){
                    $('#nav-icon').click(function(){
                        $(this).toggleClass('open');
                    });
                });
            </script>
            <!--side-panel-->

            <div class="col-lg-10 p-0">
                <!--header-->
                <div class="main-header headercolor">
                    <div class="main-header-inner">
                        <a href="{{ route('welcome')}}"><img src="{{ asset('site/img/dreamron.png?v=1.000.000.012') }}" alt="Dreamron"></a>
                    </div>
                </div>
                <!--header-->
                <div class="dreamron-common dragging-containerx d-none d-sm-block">
                    <div class="hideMe">
                        <svg id="downArrow" class="arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
                            <g stroke-linejoin="round" stroke-linecap="round">
                                <circle r="46" cx="50" cy="50"/>
                                <polyline points="25 40, 50 70, 75 40"></polyline>
                            </g>
                        </svg>
                        <svg id="upArrow" class="arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
                            <g stroke-linejoin="round" stroke-linecap="round">
                                <circle r="46" cx="50" cy="50"/>
                                <polyline points="25 60, 50 30, 75 60"></polyline>
                            </g>
                        </svg>
                        <div id="masterWrap">
                            <div id="panelWrap">
                                <section class="row m-0">
                                    <div class="col-md-6 p-0 tns-outer dreamron-text-slider">
                                    <div class="tns-item slider-text-item-container">
                                        <div class="slider-text-container">
                                            <div class="text-container top-up">
                                                <div class="bred-crumb"><span>Production & Laboratories</span> <i class="fa fa-arrow-right"></i> <span>Production</span></div>
                                                <h4>Production</h4>


                                                <p>Dreamron manufacturing possess ISO 9001:2015 quality certificate and practice TQM and 5S systems and has been awarded with GMP certificates. Dreamron Group factory complex is established in a 10-acre land with its own energy generation units, and waste water treatment plant. 

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                        <div class="dreamron-item dreamron-rnd1">

                                        </div>
                                    </div>
                                </section>
                                <section class="row m-0">
                                    <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                    <div class="tns-item slider-text-item-container">
                                        <div class="slider-text-container">
                                            <div class="text-container top-up">
                                                <div class="bred-crumb"><span>Production & Laboratories</span> <i class="fa fa-arrow-right"></i> <span>Production</span></div>
                                                
                                                <h4>Production 
                                                </h4>

                                                <p>
                                                    Our Manufacturing capabilities also include 16 tons mixing capacity with ability to fill over 100,000 cosmetic units per day in multiple pack sizes.
                                                     
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                        <div class="dreamron-item dreamron-rnd2">

                                        </div>
                                    </div>
                                </section>
                                <section class="row m-0">
                                    <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                    <div class="tns-item slider-text-item-container">
                                        <div class="slider-text-container">
                                            <div class="text-container top-up">
                                                <div class="bred-crumb"><span>Production & Laboratories</span> <i class="fa fa-arrow-right"></i> <span>Laboratories</span></div>
                                                
                                                <h4>Laboratories 
                                                </h4>

                                                <p> The central laboratory of the Dreamron group companies located inside its factory complex in Horana, is comprised of highly qualified professional scientists and chemists who are committed to produce high quality products for its customers worldwide. The laboratory is equipped with modern instruments and technology with affiliation to leading cosmetic laboratories globally such as in UK, USA, Japan to test, innovate and cater to the customer needs. Our product quality is complied with EU and Japanese standards and raw material used meet standards such as REACH and FDA. 
                                                     </p>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                        <div class="dreamron-item dreamron-rnd3">

                                        </div>
                                    </div>
                                </section>
                                <section class="row m-0">
                                    <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                    <div class="tns-item slider-text-item-container">
                                        <div class="slider-text-container">
                                            <div class="text-container top-up">
                                                <div class="bred-crumb"><span>Production & Laboratories</span> <i class="fa fa-arrow-right"></i> <span>Laboratories</span></div>
                                                
                                                <h4>Laboratories
                                                </h4>

                                                <p> Dreamron group is one of the few cosmetic manufacturers in Sri Lanka to have an accredited in-house Microbiology laboratory. It has been an additional benefit to identify and ensure product safety and compliance of the cosmetics produced in the two factories. 
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                        <div class="dreamron-item dreamron-rnd4">

                                        </div>
                                    </div>
                                </section>
                                <section class="row m-0">
                                    <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                    <div class="tns-item slider-text-item-container">
                                        <div class="slider-text-container">
                                            <div class="text-container top-up">
                                                <div class="bred-crumb"><span>Production & Laboratories</span> <i class="fa fa-arrow-right"></i> <span>Quality Policy</span></div>
                                                
                                                <h4>Quality Policy
                                                </h4>

                                                <p> The Dreamron quality assurance has been rated very high by the respective international independent laboratories which test our products. The quality assurance team at Dreamron is well qualified and trained to ensure 100% product quality. 
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                        <div class="dreamron-item dreamron-rnd5">

                                        </div>
                                    </div>
                                </section>

                            </div>
                        </div>
                        <div class="dots">
                        </div>
                        <div class="toolTips">
                            <div class="toolTip">
                                Production
                            </div>
                            <div class="toolTip">
                                Production
                            </div>
                            <div class="toolTip">
                                Laboratories
                            </div>
                            <div class="toolTip">
                                Laboratories
                            </div>
                            <div class="toolTip">
                                Quality Policy
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-block d-sm-none">
                            
                    <section class="row m-0 dreamron-item dreamron-rnd1">
                        <div class="col-md-6 p-0 tns-outer" style="background-color: aliceblue; opacity:0.9">
                        <div class="tns-item slider-text-item-container">
                            <div class="slider-text-container">
                                <div class="text-container top-up">
                                    <div class="bred-crumb"><span>Production & Laboratories</span> <i class="fa fa-arrow-right"></i> <span>Production</span></div>
                                    <h4>Production</h4>


                                    <p>Dreamron manufacturing possess ISO 9001:2015 quality certificate and practice TQM and 5S systems and has been awarded with GMP certificates. Dreamron Group factory complex is established in a 10-acre land with its own energy generation units, and waste water treatment plant. 

                                    </p>
                                </div>
                            </div>
                        </div>
                        </div>
                    </section>
                    <section class="row m-0 dreamron-item dreamron-rnd2">
                        <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.9">
                        <div class="tns-item slider-text-item-container">
                            <div class="slider-text-container">
                                <div class="text-container top-up">
                                    <div class="bred-crumb"><span>Production & Laboratories</span> <i class="fa fa-arrow-right"></i> <span>Production</span></div>
                                    
                                    <h4>Production 
                                    </h4>

                                    <p>
                                        Our Manufacturing capabilities also include 16 tons mixing capacity with ability to fill over 100,000 cosmetic units per day in multiple pack sizes.
                                         
                                    </p>
                                </div>
                            </div>
                        </div>
                        </div>
                    </section>
                    <section class="row m-0 dreamron-item dreamron-rnd3">
                        <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.9">
                        <div class="tns-item slider-text-item-container">
                            <div class="slider-text-container">
                                <div class="text-container top-up">
                                    <div class="bred-crumb"><span>Production & Laboratories</span> <i class="fa fa-arrow-right"></i> <span>Laboratories</span></div>
                                    
                                    <h4>Laboratories 
                                    </h4>

                                    <p> The central laboratory of the Dreamron group companies located inside its factory complex in Horana, is comprised of highly qualified professional scientists and chemists who are committed to produce high quality products for its customers worldwide. The laboratory is equipped with modern instruments and technology with affiliation to leading cosmetic laboratories globally such as in UK, USA, Japan to test, innovate and cater to the customer needs. Our product quality is complied with EU and Japanese standards and raw material used meet standards such as REACH and FDA. 
                                         </p>
                                </div>
                            </div>
                        </div>
                        </div>
                    </section>
                    <section class="row m-0 dreamron-item dreamron-rnd4">
                        <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.9">
                        <div class="tns-item slider-text-item-container">
                            <div class="slider-text-container">
                                <div class="text-container top-up">
                                    <div class="bred-crumb"><span>Production & Laboratories</span> <i class="fa fa-arrow-right"></i> <span>Laboratories</span></div>
                                    
                                    <h4>Laboratories
                                    </h4>

                                    <p> Dreamron group is one of the few cosmetic manufacturers in Sri Lanka to have an accredited in-house Microbiology laboratory. It has been an additional benefit to identify and ensure product safety and compliance of the cosmetics produced in the two factories. 
                                    </p>
                                </div>
                            </div>
                        </div>
                        </div>
                    </section>
                    <section class="row m-0 dreamron-item dreamron-rnd5">
                        <div class="col-md-6 col-lg-5 p-0 tns-outer" style="background-color: aliceblue; opacity:0.9">
                        <div class="tns-item slider-text-item-container">
                            <div class="slider-text-container">
                                <div class="text-container top-up">
                                    <div class="bred-crumb"><span>Production & Laboratories</span> <i class="fa fa-arrow-right"></i> <span>Quality Policy</span></div>
                                    
                                    <h4>Quality Policy
                                    </h4>

                                    <p> The Dreamron quality assurance has been rated very high by the respective international independent laboratories which test our products. The quality assurance team at Dreamron is well qualified and trained to ensure 100% product quality. 
                                    </p>
                                </div>
                            </div>
                        </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    @include('partials.footer')
@endsection



@section('script')
    <script src="{{ asset('site/js/stopexecutionontimeout.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/gsap-latest-beta.min.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/draggable3.min.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/inta.mini.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/scroll-slider.js?v=1.000.000.012') }}"></script>
@endsection