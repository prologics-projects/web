@extends('layouts.site')

@section('title')
    <title>R & D - Dreamron</title>
@endsection



@section('bodyclass')
    class="to-top on-side-way"
@endsection

@section('content')
        

    <style>
        #galleryshow{
            color: #000;
            font-weight: 500;
        }
        
            
        
            @media (max-width: 575.98px) {
                .slider-text-container p, .year-val ,.year-info , .readmore-content{
                    margin-top: 0;
                    font-size: 70%;
                    line-height: 18px;
                }
                .slider-text-container h1{
                    font-size:1em;
                }
                .slider-text-container h4 , .readmore-head{
                    font-size:0.8em;
                    line-height: 16px;
                    margin-bottom: 0px;
                    padding: 0px 0px 0px 0px;
                }
                .slider-text-container p .btn-internal{
                    font-size: 80%;
                }
                .main-header .main-header-inner img{
                    max-width: 200px;
                }
            }

            @media (min-width: 576px) and (max-width: 767.98px) {
                .slider-text-container p, .year-val ,.year-info , .readmore-content{
                    margin-top: 0;
                    font-size: 70%;
                    line-height: 18px;
                }
                .slider-text-container h1{
                    font-size:1em;
                }
                .slider-text-container h4 , .readmore-head{
                    font-size:0.8em;
                    line-height: 16px;
                    margin-bottom: 0px;
                    padding: 0px 0px 0px 0px;
                }
                .slider-text-container p .btn-internal{
                    font-size: 80%;
                }
                .main-header .main-header-inner img{
                    max-width: 200px;
                }
            }

            @media (min-width: 768px) and (max-width: 991.98px) {
                .slider-text-container p, .year-val ,.year-info , .readmore-content{
                    margin-top: 0;
                    font-size: 70%;
                    line-height: 18px;
                }
                .slider-text-container h1{
                    font-size:1.2em;
                }
                .slider-text-container h4 , .readmore-head{
                    font-size:1em;
                    line-height: 18px;
                    margin-bottom: 0px;
                    padding: 0px 0px 0px 0px;
                }
                .slider-text-container p .btn-internal{
                    font-size: 80%;
                }
                .main-header .main-header-inner img{
                    max-width: 200px;
                }
            }

            @media (min-width: 992px) and (max-width: 1299.98px) {
                .slider-text-container p, .fourpillarsp{
                    margin-top: 0;
                    font-size: 80%;
                    line-height: 18px;
                }
                .slider-text-container h1{
                    font-size:1.2em;
                }
                .slider-text-container h4 , .readmore-head{
                    font-size:1em;
                    line-height: 18px;
                    margin-bottom: 0px;
                    padding: 0px 0px 0px 0px;
                }
                .slider-text-container p .btn-internal{
                    font-size: 80%;
                }
                .main-header .main-header-inner img{
                    max-width: 200px;
                }
            }

            @media (min-width: 1300px) and (max-width: 1599.98px) {
                .slider-text-container p , .year-val ,.year-info , .readmore-content{
                    margin-top: 0;
                    font-size: 80%;
                    line-height: 18px;
                }
                .navbar-nav{
                    font-size: 80%;
                }
                .slider-text-container h1 {
                    font-size:1.3em;

                    
                }
                .slider-text-container h4 , .readmore-head{
                    font-size:1em;
                    line-height: 18px;
                    margin-bottom: 0px;
                    padding: 0px 0px 0px 0px;
                }
                .slider-text-container p .btn-internal{
                    font-size: 80%;
                }
                .main-header .main-header-inner img{
                    max-width: 200px;
                }
            }
            h1,h4{
                color:#211651;
            }
    </style>
    <div class="container-fluid">
        <div class="row">
            <!--side-panel-->
            <div class="col-lg-2 p-0 main-side-panel">
                @include('partials.search')

                <nav class="navbar navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <!--<span class="navbar-toggler-icon"></span>-->
                        <div id="nav-icon" class="nav-icon">
                            <div class="nav-icon-inner">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </button>

                    @include('partials.sidebar')
                </nav>
            </div>

            <script>
                $(document).ready(function(){
                    $('#nav-icon').click(function(){
                        $(this).toggleClass('open');
                    });
                });
            </script>
            <!--side-panel-->

            <div class="col-lg-10 p-0">
                <!--header-->
                <div class="main-header">
                    <div class="main-header-inner">
                        <a href="{{ route('welcome')}}"><img src="{{ asset('site/img/dreamron.png?v=1.000.000.012') }}" alt="Dreamron"></a>
                    </div>
                </div>
                <!--header-->

                <script src="https://cdn.jsdelivr.net/npm/publicalbum@latest/embed-ui.min.js" async></script>
                <div class="pa-carousel-widget" style="width:100%; height:100vh; display:none;"
                  data-link="https://photos.app.goo.gl/fNXz4L86AFLf6KM87"
                  data-title="Dreamron"
                  data-description="15 new photos added to shared album"
                  data-background-color="transparent">
                  <object data="https://lh3.googleusercontent.com/RHjF2CVVDAFxDGrAC_BoTbD_bxRGGfB3xKASLzu2Tw3CdDcD_ptRQpa9_Tvgjx9XTCgEesBlKMx__lkGLyKQ_NFx0vAUKuGkTVQrhX8cCQwlTvCwcVVCyA-sulmW7872W7aKuf9S=w1920-h1080"></object>
                  <object data="https://lh3.googleusercontent.com/RF42yNl1SddfXYokHO3w-sVEiCiXOUXCJdfdV-yEwB8I2poXIX397TAC2ugPBGO_MmHglISZueZ-rwCe2LvUU_dPlNEbOtDu09dVox5nBM84zKIOWpwPiJNyK7IZ74Q2Xtga58VW=w1920-h1080"></object>
                  <object data="https://lh3.googleusercontent.com/h2_CNyT6ZZMD9GL2ckrtGva9--ZBf1iuU5UzRsbObO9daW75AcxUx6auWtL78dRIXc46019StiUvt4JB3RyxcmweIhMV5CIS4GfgdCFkSGqOYaoVwl74Y2YOKLheZwqyiUpdakVu=w1920-h1080"></object>
                  <object data="https://lh3.googleusercontent.com/Pe1BPEH2z5lJrv0tr0lAhjnyLeouBbkxXw1m3ihW2kuBfbH6mkgikXLn5rsYTszwA3khjQy97LucPGim64gHOhWse52dtn7RT1tfNn1vbKLD59Sr6t-ZsXAF0jbKjSE2S5vYM3yL=w1920-h1080"></object>
                  <object data="https://lh3.googleusercontent.com/rRtHJllH8sLIoR5s7lH4geW8Sq8EEfBDmLLW6kG1C3gP_vxo9VOaGNjmYLn1IaMWjvSIgvn5uIkGkbeVdz5JvBC1RFrUUuVzGcSkw9YF4KW9uRUMVpp7Tot9TBrofsDSfliyIMHk=w1920-h1080"></object>
                  <object data="https://lh3.googleusercontent.com/wZUWoSNAnaLCxP4rkNCOZsj35hfE4VNJX_V0tbbmPB6kSaDxQhzKKnQQYunnKSzFhSj2b1butNgKtuNBey1BD9bcPJ1w9morB0-2iWkApLhSRTwoMGTmC_BKIeonQ1dsVjpKdrau=w1920-h1080"></object>
                  <object data="https://lh3.googleusercontent.com/be15reHF4lJveDC5q8EiHJvlzbM6GzZ52uSiAXWZBO3suJkDySsc3jMELOMRJE33mCw-DdixRAxqZa2TVViLlYdXb3ksm5-9VQ6bZy5ycR0nJlP-so-h0YS-GI8xq29m5FYHEXw3=w1920-h1080"></object>
                  <object data="https://lh3.googleusercontent.com/ng18V_aQcpFj6g8fs78CTY7t6uInUVNLWLVixZq10cyZdOBXjhNkRipGfyHd5cle7q_A52SsnCqoLJXT8fCXXLGKEJNyQnXuBqDI1JzHjzjnciG5Y61IkWo3j0cxmaY7VgvukZj8=w1920-h1080"></object>
                  <object data="https://lh3.googleusercontent.com/vnazvaexQ6IdZ31Lic0Ol11rbY_P0Dc_cDmmeW480MNiz9A2eF4F3e5t7QbYpjKSCPcODCZXYtMqm_9sYvs1_UGhRUF3p7cK0N9zUzgtfQLLOjL_zoBZVetZ6KGfimOTxEinjzes=w1920-h1080"></object>
                  <object data="https://lh3.googleusercontent.com/mvSu0aYWMcrNgu0uUKcMD_myaXGDag3ZjZDAQ6wGvuhVAQtv7LLta79nO1NBDNURWruaAtTfTES1VbMHnWh7Ev3aWX5scl_WV9MDrjvyz3HCmGaMMl_qm8eqz9EwxbdhA-Spx30S=w1920-h1080"></object>
                  <object data="https://lh3.googleusercontent.com/j2MrDvJaL57nKdSulf5e2SyEp01erm8xt_UdeC5qfTyp6BF-7Vwyuwc9EH3hjvY_wE_Kn_pYgi8Z37bd0-6s7MWOj__QUKnp3a5efVG8dF96DGOaeaxSN0pZVjTbxzsA5MkTGa9F=w1920-h1080"></object>
                  <object data="https://lh3.googleusercontent.com/2Wn-2kcml5JDRkJ5GnaBVvTOjmWcN1HhgLe53l0tuWiAHi0zq_tBJNgfg0BwnDct-PNyn2IlE7fnz4nbRCOnUn-_GxLW6QEyA7wtdbdx9_mZaOlgY8fVZKg-tNVGGXrAXjgFgvu8=w1920-h1080"></object>
                  <object data="https://lh3.googleusercontent.com/b6yEwoSHdOvMjpfO4U66tkN-L-qxryI7w_QynCxSmfR4naA4XXIAnjCSJ6PiovaAP1TrMJCx9akVNgVOeo2zi-ke_Z9a9GAa6gKoZTlAE3BU62oShf1j2vs1Rg3gOe8yxVzrXYXw=w1920-h1080"></object>
                  <object data="https://lh3.googleusercontent.com/SwDFPt2Wj23MDbL2KroxrjJbUXGe431BcobMQu0ztBIP978geV6Mx3_vAUWP-tt8WSo9vQqT2F_4zecdRaFehfUMDABE1BBem-jmT8LHyVqVTwFw4jmACc2rZulqH8sLFGE53c0K=w1920-h1080"></object>
                  <object data="https://lh3.googleusercontent.com/JQ5P7OQomj--iMaiKlS4oVOQg7psUS1SIykUMnk-67YipmUNcy9WpwGL7uATqAjVLW_aLIsgRBaEFe-6cWoIEwVIOHuzsC7FEpJv9gOOhtFN6nPPIrn-AOVGAN3D2vbp5jVrTwgW=w1920-h1080"></object>
                </div>
                

            </div>
        </div>
    </div>

    <!--footer-->
    <div class="main-footer">
        
        
        
        <ul class="social-icons">
            <li>
                <a href="https://www.facebook.com/dreamronsrilanka/" target="_blank">
                    <i class="fa fa-facebook"></i>
                </a>
            </li>
            <li>
                <a href="https://www.instagram.com/dreamronsrilanka" target="_blank">
                    <i class="fa fa-instagram"></i>
                </a>
            </li>
            <li>
                <a href="#" target="_blank">
                    <i class="fa fa-twitter"></i>
                </a>
            </li>
        </ul>
    </div>
    <!--/.footer-->
@endsection



@section('script')
    <script src="{{ asset('site/js/stopexecutionontimeout.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/gsap-latest-beta.min.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/draggable3.min.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/inta.mini.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/scroll-slider.js?v=1.000.000.012') }}"></script>
@endsection