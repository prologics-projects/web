@extends('layouts.site')

@section('title')
    <title>News Room - Dreamron</title>
@endsection



@section('bodyclass')
    class="to-top  on-side-way"
@endsection

@section('content')
    

        <style>
            .date {
                left:0;
                background:blue;
                display: inline-block;
                position: absolute;
                padding: 10px;
                top:10px;
                margin-top: -10px;
            }
            .borderone{
                border-top-color: #002fff !important;
            }
            #oem{
                color: #000;
                font-weight: 500;
            }

            
            
            @media (max-width: 575.98px) {
                .slider-text-container p, .year-val ,.year-info , .readmore-content{
                    margin-top: 0;
                    font-size: 70%;
                    line-height: 18px;
                }
                .slider-text-container h1{
                    font-size:1em;
                }
                .slider-text-container h4 , .readmore-head{
                    font-size:0.8em;
                    line-height: 16px;
                    margin-bottom: 0px;
                    padding: 0px 0px 0px 0px;
                }
                .slider-text-container p .btn-internal{
                    font-size: 80%;
                }
                .main-header .main-header-inner img{
                    max-width: 200px;
                }
            }

            @media (min-width: 576px) and (max-width: 767.98px) {
                .slider-text-container p, .year-val ,.year-info , .readmore-content{
                    margin-top: 0;
                    font-size: 70%;
                    line-height: 18px;
                }
                .slider-text-container h1{
                    font-size:1em;
                }
                .slider-text-container h4 , .readmore-head{
                    font-size:0.8em;
                    line-height: 16px;
                    margin-bottom: 0px;
                    padding: 0px 0px 0px 0px;
                }
                .slider-text-container p .btn-internal{
                    font-size: 80%;
                }
                .main-header .main-header-inner img{
                    max-width: 200px;
                }
            }

            @media (min-width: 768px) and (max-width: 991.98px) {
                .slider-text-container p, .year-val ,.year-info , .readmore-content{
                    margin-top: 0;
                    font-size: 70%;
                    line-height: 18px;
                }
                .slider-text-container h1{
                    font-size:1.2em;
                }
                .slider-text-container h4 , .readmore-head{
                    font-size:1em;
                    line-height: 18px;
                    margin-bottom: 0px;
                    padding: 0px 0px 0px 0px;
                }
                .slider-text-container p .btn-internal{
                    font-size: 80%;
                }
                .main-header .main-header-inner img{
                    max-width: 200px;
                }
            }

            @media (min-width: 992px) and (max-width: 1299.98px) {
                .slider-text-container p, .fourpillarsp{
                    margin-top: 0;
                    font-size: 80%;
                    line-height: 18px;
                }
                .slider-text-container h1{
                    font-size:1.2em;
                }
                .slider-text-container h4 , .readmore-head{
                    font-size:1em;
                    line-height: 18px;
                    margin-bottom: 0px;
                    padding: 0px 0px 0px 0px;
                }
                .slider-text-container p .btn-internal{
                    font-size: 80%;
                }
                .main-header .main-header-inner img{
                    max-width: 200px;
                }
            }

            @media (min-width: 1300px) and (max-width: 1599.98px) {
                .slider-text-container p , .year-val ,.year-info , .readmore-content{
                    margin-top: 0;
                    font-size: 80%;
                    line-height: 18px;
                }
                .navbar-nav{
                    font-size: 80%;
                }
                .slider-text-container h1 {
                    font-size:1.3em;

                    
                }
                .slider-text-container h4 , .readmore-head{
                    font-size:1em;
                    line-height: 18px;
                    margin-bottom: 0px;
                    padding: 0px 0px 0px 0px;
                }
                .slider-text-container p .btn-internal{
                    font-size: 80%;
                }
                .main-header .main-header-inner img{
                    max-width: 200px;
                }
            }
            h1,h4{
                color:#211651;
            }

            @media only screen and (max-width: 600px) 
            {


                .fixed-header{
                    position: fixed;        
                }
            }
            
            @media only screen and (max-width: 600px) 
            {


                .headercolor{
                    background-color: rgb(255, 255, 255);
                    padding-top: -10px;
                    padding-bottom: 63px;
                    z-index: 1;
                            
                }
            }

            .timeline {
                list-style-type: none;
                display: flex;
                align-items: center;
                justify-content: center;
                background-repeat: no-repeat;
            }

.li {
  transition: all 200ms ease-in;
}

.timestamp {
  margin-bottom: 20px;
  padding: 0px 40px;
  display: flex;
  flex-direction: column;
  align-items: center;
}

.status {
  padding: 0px 40px;
  display: flex;
  justify-content: center;
  border-top: 1px solid #D6DCE0;
  position: relative;
  transition: all 200ms ease-in;
}

.status:before {
  content: "";
  width: 15px;
  height: 15px;
  background-color: white;
  border-radius: 25px;
  border: 1px solid #ddd;
  position: absolute;
  top: -15px;
  left: 42%;
  transition: all 200ms ease-in;
}

.li.complete .status {
  border-top: 2px solid #ffffff;
}
.li.complete .status:before {
  background-color: #ffffff;
  border: none;
  transition: all 200ms ease-in;
}
.li .complete .status h4 {
  color: #ffffff;
}

@media (min-device-width: 320px) and (max-device-width: 700px) {
  .timeline {
    list-style-type: none;
    display: block;
  }

  .li {
    transition: all 200ms ease-in;
    display: flex;
    width: inherit;
  }

  .timestamp {
    width: 50px;
  }

  .status:before {
    left: -8%;
    top: 30%;
    transition: all 200ms ease-in;
  }
}

@media only screen and (max-width: 600px) 
            {

            .datecircle{
            padding-top: 120px;
                            
            }

}

@media only screen and (max-width: 600px) 
            {


                .oveltextcolor{
                    margin-top:50px;
                }
            }

.ex3 {
  overflow-y: auto;
  overflow-x: hidden;
}

@media (min-width:1025px) { 
    .main-header {
    width: calc(100% - 35px);
    z-index: 100;
    background: linear-gradient(
            50deg,
            #b5a0be 0,
            #b19ae7 40%,
            #c3a3f8 100%
        );
}
}
@media (min-width:1281px) { 
    .main-header {
    width: calc(100% - 35px);
    z-index: 100;
    background: linear-gradient(
            50deg,
            #b5a0be 0,
            #b19ae7 40%,
            #c3a3f8 100%
        );
}
 }



.center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
}

@media only screen and (max-width: 600px) 
            {


                .mobilebackcolor{
                    background: linear-gradient(
            50deg,
            #b5a0be 0,
            #b19ae7 40%,
            #c3a3f8 100%
        );
                }

            }

@media only screen and (max-width: 600px) 
            {

            .timelinehide{
                display: none;
            }

            }


 </style>
        <div class="container-fluid fixed">
            <div class="row headercolor">
                <!--side-panel-->
                <div class="col-lg-2 p-0 main-side-panel fixed-header">
                    @include('partials.search')

                    <nav class="navbar navbar-expand-lg navbar-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <!--<span class="navbar-toggler-icon"></span>-->
                            <div id="nav-icon" class="nav-icon">
                                <div class="nav-icon-inner">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </button>

                        @include('partials.sidebar')
                    </nav>
                </div>

                <script>
                    $(document).ready(function(){
                        $('#nav-icon').click(function(){
                            $(this).toggleClass('open');
                        });
                    });
                </script>
                <!--side-panel-->

                <div class="col-lg-10 p-0 mobilebackcolor">
                    <!--header-->
                        <div class="main-header headercolor">
                            <div class="main-header-inner" align="center">
                                <a href="{{ route('welcome')}}"><img src="{{ asset('site/img/dreamron.png?v=1.000.000.012') }}" alt="Dreamron" class="center"></a>
                            </div>
                        </div>
                        
                    <!--header-->

                    <div class="dreamron-common dragging-containerx datecircle ex3">
                        <div class="ml-md-5 mt-md-5" style="padding-top:10vw">
                            <div class="d-flex justify-content-center">
                            <div class="col-md-4 col-8 row  border border-dark rounded-pill d-flex justify-content-center oveltextcolor xs-mt-6">
                                <div class="col-md-12 d-flex justify-content-center">
                                    <h3><u><center>Year</center></u></h3>
                                </div>
                                <div class="row col-md-12 d-flex justify-content-center">
                                    <div class="col-4 col-md-4">
                                    <a class="col-md-4" href="{{ route('pastyear')}}" id="2020" value="2020" onClick="reply_click(this.id)"><h4 class="text-danger">2020</h4></a>
                                    </div>
                                    <div class="col-4 col-md-4">
                                    <a class="col-md-4" href="{{ route('thisyear')}}" id="2021" value="2021" onClick="reply_click(this.id)"><h4 class="text-danger">2021</h4></a>
                                    </div>
                                    <div class="col-4 col-md-4">
                                    <a class="col-md-4" href="{{ route('nextyear')}}" id="2022" value="2022" onClick="reply_click(this.id)"><h4 class="text-danger">2022</h4></a>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row d-block d-sm-none col-12 d-flex justify-content-center ml-1 mt-2">
                                    <div class="col-8 border border-dark rounded-pill">
                                    <select class="form-control my-4">
                                        <option>Month</option>
                                        <option month="01" class="btn-month mbtn">January</option>
                                        <option month="02" class="btn-month mbtn">February</option>
                                        <option month="03" class="btn-month mbtn">March</option>
                                        <option month="04" class="btn-month mbtn">April</option>
                                        <option month="05" class="btn-month mbtn">May</option>
                                        <option month="06" class="btn-month mbtn">June</option>
                                        <option month="07" class="btn-month mbtn">July</option>
                                        <option month="08" class="btn-month mbtn">August</option>
                                        <option month="09" class="btn-month mbtn">September</option>
                                        <option month="10" class="btn-month mbtn">October</option>
                                        <option month="11" class="btn-month mbtn">November</option>
                                        <option month="12" class="btn-month mbtn">December</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="col-md-12 d-flex justify-content-center  d-none d-sm-block d-sm-none d-md-block timelinehide" style="margin-top: 50px;">
                                    <ul class="timeline timelinehide" id="timeline">
                                        <li class="li complete">
                                            <div class="status">
                                                <a href="#" month="01" class="btn-month mbtn"><h4 class="text-white"> Jan </h4></a>
                                            </div>
                                        </li>
                                        <li class="li complete">
                                            <div class="status">
                                                <a href="#" month="02" class="btn-month mbtn"><h4 class="text-white"> Feb </h4></a>
                                            </div>
                                        </li>
                                        <li class="li complete">
                                            <div class="status">
                                                <a href="#" month="03" class="btn-month mbtn"><h4 class="text-white"> Mar </h4></h4></a>
                                            </div>
                                        </li>
                                        <li class="li complete">
                                            <div class="status">
                                                <a href="#" month="04" class="btn-month mbtn"><h4 class="text-white"> Apr </h4></a>
                                            </div>
                                        </li>
                                        <li class="li complete">
                                            <div class="status">
                                                <a href="#" month="05" class="btn-month mbtn"><h4 class="text-white"> May </h4></a>
                                            </div>
                                        </li>
                                        <li class="li complete">
                                            <div class="status">
                                                <a href="#" month="06" class="btn-month mbtn"><h4 class="text-white"> Jun </h4></a>
                                            </div>
                                        </li>
                                        <li class="li complete">
                                            <div class="status">
                                                <a href="#" month="07" class="btn-month mbtn"><h4 class="text-white"> Jul </h4></a>
                                            </div>
                                        </li>
                                        <li class="li complete">
                                            <div class="status">
                                                <a href="#" month="08" class="btn-month mbtn"><h4 class="text-white"> Aug </h4></a>
                                            </div>
                                        </li>
                                        <li class="li complete">
                                            <div class="status">
                                                <a href="#" month="09" class="btn-month mbtn"><h4 class="text-white"> Sep </h4></a>
                                            </div>
                                        </li>
                                        <li class="li complete">
                                            <div class="status">
                                                <a href="#" month="10" class="btn-month mbtn"><h4 class="text-white"> Oct </h4></a>
                                            </div>
                                        </li>
                                        <li class="li complete">
                                            <div class="status">
                                                <a href="#" month="11" class="btn-month mbtn"><h4 class="text-white"> Nov </h4></a>
                                            </div>
                                        </li>
                                        <li class="li complete">
                                            <div class="status">
                                                <a href="#" month="12" class="btn-month mbtn"><h4 class="text-white"> Dec </h4></a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                
                            <div id="" style="margin-top: 50px;" class="col-md-12 col-12 col-sm-12">
                                <div id="" class="" class="col-md-12 col-12 col-sm-12">
                                    <div class="row col-12 col-md-12">
                                        @foreach($blogs as $blog)
                                            <div class="col-12 col-sm-12 col-md-4 post-{{ substr($blog->published_at,5,2) }} post mb-3 mt-xs-5" style="">
                                                <div class="borderone border1 border-dark rounded  pt-2 pb-2">
                                                    <div class="date" ><h5  class="text-white">{{ substr($blog->published_at,5,10) }}</h5></div>
                                                    <h4 style="margin-left: 100vw"><b>{{ substr($blog->title,0,11) }}</b></h4>
                                                    <div>
                                                        <div align="center">
                                                            <img src="{{ asset('storage/'.$blog->image) }}" class="img-fluid" style="overflow: hidden;" height="180vh" width="350vh" alt="">
                                                        </div>
                                                        <br>
                                                        <div class="m-3">
                                                            <h6><b>{{ substr($blog->description,0,500) }}</b></h6>
                                                        </div>
                                                        <div class="ml-3">
                                                            <a class="btn btn-md border border-dark rounded-pill" href={{route('view',$blog->id)}} >View More </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('partials.footer')
@endsection


@section('script')
    <script src="{{ asset('site/js/stopexecutionontimeout.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/gsap-latest-beta.min.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/draggable3.min.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/inta.mini.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/scroll-slider.js?v=1.000.000.012') }}"></script>
@endsection

<script src="//code.jquery.com/jquery-1.11.3.js"></script>

<script>
    $(document).on("click",".btn-month",function() {
        let month=$(this).attr("month")
        $(".post").fadeOut(800)
        // $(".post-01").fadeIn(350)
        $(".post-"+month).fadeIn(400)
    })
</script>