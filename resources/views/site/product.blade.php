<!DOCTYPE html>
<html lang="en">

<!--head-->
<head>
    <meta charset="UTF-8">
    <title>Products - Dreamron</title>
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--Favicon -->
    <link rel="shortcut icon" type='image/x-icon' href="{{ asset('site/img/favicon/favicon.ico?v=1.000.000.012') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('site/img/favicon/apple-touch-icon.png?v=1.000.000.012') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('site/img/favicon/favicon-32x32.png?v=1.000.000.012') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('site/img/favicon/favicon-16x16.png?v=1.000.000.012') }}">
    <link rel="manifest" href="{{ asset('site/img/favicon/site.webmanifest?v=1.000.000.012') }}">
    <link rel="mask-icon" href="{{ asset('site/img/favicon/safari-pinned-tab.svg?v=1.000.000.012') }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="{{ asset('site/css/tiny-slider.css?v=1.000.000.012') }}">
    <!--[if (lt IE 9)]><script src="js/tiny-slider.helper.ie8.js"></script><![endif]-->
    <script src="js/tiny-slider.js"></script>

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('site/css/bootstrap.css?v=1.000.000.012') }}" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('site/css/aos.css?v=1.000.000.012') }}" crossorigin="anonymous">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{ asset('site/css/fontawesome.css?v=1.000.000.012') }}" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('site/css/style.css?v=1.000.000.012') }}" crossorigin="anonymous">

    <!-- jQuery JS-->
    <script src="{{ asset('site/js/jquery.js?v=1.000.000.012') }}" crossorigin="anonymous"></script>
</head>
<!--/.head-->

<body id="expander" class="to-top">
<style>
    #home {
        color: #000;
        font-weight: 500;
    }
    
    h1,h4{
        color:#211651;
    }
    
    @media only screen and (max-width: 600px) 
            {


                .hidemobile{
                    display:none;
                            
                }
            }
            
</style>


<div class="container-fluid dreamron-products">
    <button class="navbar-toggler">
        <div id="nav-icon" class="nav-icon">
            <div class="nav-icon-inner">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </button>
    <div class="row">
        <div class="col-sm-4 col-md-3 col-xl-2 product-category-side-panel">
            <div class="product-category-side-panel-inner">
                <a href="index.html" class="brand-logo">
                    <img src="{{ asset('site/img/dreamron.png') }}" alt="">
                </a>

                <div class="collapse show">
                    <ul class="nav">
                        <li class="main-nav-controller">
                            <span class="back" onclick="goBack()"><i class="fa fa-arrow-left"></i>Back</span>
                            <a class="home" href="{{ route('homepage')}}"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li class="main-category">
                            Brands
                        </li>
                        @foreach ($brands as $brand)
                            <li class="sub-category">
                                <a  class="get-products" data-brand_id="{{ $brand->id }}" data-category_id="" data-subcategory_id="">{{ $brand->name }}</a>
                            </li>
                        @endforeach
                        <li class="main-category">
                            Categories
                        </li>
                        @foreach ($categories as $category)
                            @if ($category->subcategories->count()>0)
                                <li class="nav-item">
                                    <a class="collapsed nav-link" href="#cat{{$category->id}}" data-toggle="collapse"
                                    data-target="#cat{{$category->id}}">{{$category->name }}
                                        <i class="fa fa-chevron-down"></i>
                                    </a>
        
                                    <div class="collapse sub-collapse" id="cat{{$category->id}}" aria-expanded="false">
                                        <ul class="flex-column">
                                            @foreach ($category->subcategories as $subcategory)
                                                <li class="nav-item">
                                                    <a   class="get-products" data-brand_id="" data-category_id="" data-subcategory_id="{{ $subcategory->id }}"><i class="fa fa-angle-right"></i>{{$subcategory->name}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>
                            @else
                                <li class="nav-item">
                                    <a  class="get-products" data-brand_id="" data-category_id="{{ $category->id }}" data-subcategory_id="">{{ $category->name}}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <div class="products-category-footer">
                    <ul class="social-icons">
                        <li>
                            <a href="" target="_blank">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="" target="_blank">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="" target="_blank">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-8 col-md-9 col-xl-10 main-products">
            @if (!isset($emptymessage))
                
                <div class="unique-product-container">
                    <button class="navbar-toggler">
                        <div id="nav-icon-prod" class="nav-icon open">
                            <div class="nav-icon-inner">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </button>
                    <div id="carouselExampleControls" class="carousel-product carousel slide h-100" data-ride="carousel">
                        <div class="carousel-inner h-100">
                            @foreach ($products as $product)
                            <div class="productcarousel carousel-item h-100" data-slide="{{ $product->id }}" >
                                <div class="row carousel-product-inner">
                                    <div class="col-12 col-md-5 carousel-img">
                                        <img class="d-block w-100" src="{{ asset('storage/'.$product->image)}}" alt="First slide">
                                    </div>
                                    <div class="col-12 col-md-7 carousel-disc" >
                                        <h3>{{ $product->name }}</h3>

                                        <p>{{ $product->description }}</p>

                                        <div class="product-info">
                                                @if ($product->amount)
                                                    <span class="product-info-group">
                                                        <span class="product-info-label">Amount</span>
                                                        <span class="product-info-value">{{ $product->amount }}</span>
                                                    </span>
                                                @endif
                                                @if ($product->sku)
                                                <!--
                                                <span class="product-info-group">
                                                    <span class="product-info-label">SKU</span>
                                                    <span class="product-info-value">{{ $product->sku }}</span>
                                                </span>
                                                -->
                                                @endif
                                                <span class="product-info-group">
                                                    <span class="product-info-label">Category</span>
                                                    <span class="product-info-value">{{ $product->category->name }}</span>
                                                </span>
                                                @if (isset($product->subcategory))
                                                    <span class="product-info-group">
                                                        <span class="product-info-label">Sub Category</span>
                                                        <span class="product-info-value">{{ $product->subcategory->name }}</span>
                                                    </span>
                                                @endif
                                        </div>

                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="row all-product-container">
                    @foreach ($products as $product)

                        <div class="col col-sm-6 col-md-4 col-lg-3 a-product productclick" data-item="{{ $product->id }}">
                            <div class="product-inner  " >
                                <img src="{{ asset('storage/'.$product->image)}}" class="" alt="product name" style="width: 120px">

                                <p class="product-name">{{ $product->name }}</p>
                                @if ($product->amount)
                                <p>Rs. {{ $product->amount }}/=</p>
                                @else
                                <p>{{ $product->category->name }}</p>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="main-progress-bar all-products-footer hidemobile">
                    <div class="brand-bar bar-left"></div>
                    <div class="brand-bar bar-right"></div>
                </div>
            @else
                
            <div style=" 
                height: calc(100vh - 70px);
                overflow: auto;margin: 0 auto; 
                display: flex;
                align-items: center;
                justify-content: center;
                flex-direction: column;">
                <h1 class="" style="color:#D11875 ; font-size:520%">No Products Available.</h1>
            </div>
            @endif
        </div>
    </div>
</div>


<!--foot-->
<!-- Popper.js, then Bootstrap JS-->
<script src="{{ asset('site/js/vendors.js?v=1.000.000.012') }}" crossorigin="anonymous"></script>
<!-- Custom js calling index.js-->
<script src="{{ asset('site/js/index.js?v=1.000.000.012') }}" crossorigin="anonymous"></script>


<!--/.foot-->

<script>
    $(document).ready(function () {

        $(document).on('click','.get-products',function () {
            var brand_id = $(this).data('brand_id');
            var category_id = $(this).data('category_id');
            var subcategory_id = $(this).data('subcategory_id');
            $.ajax({
                url:"{{ route('loadproducts') }}",
                data:{
                    'brand_id':brand_id,
                    'category_id':category_id,
                    'subcategory_id':subcategory_id,
                },
                success:function(data)
                {
                    $(".main-products").empty();
                    $ (".main-products").append (data).fadeIn (2000);
                }
            })
        });

        $(document).on('click','.a-product',function (e) {
            var dataitem = $(this).data('item');
            console.log(dataitem);
            $('.productcarousel').removeClass('active');
            $('.productcarousel[data-slide='+dataitem+']').addClass('active');
            $(".unique-product-container").addClass("open-panel");
        });


        $(document).on('click','#nav-icon',function () {
            $(this).toggleClass('open');
            $(".product-category-side-panel").toggleClass("open-menu");
            $("#nav-icon").toggleClass("on-white");
        });

        $(document).on('click','.a-product',function () {
            $(".unique-product-container").addClass("open-panel");
        });

        $(document).on('click','#nav-icon-prod',function () {
            $(".unique-product-container").removeClass("open-panel");
        });
    });
</script>
</body>

</html>