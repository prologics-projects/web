@extends('layouts.site')

@section('title')
    <title>OEM - Dreamron</title>
@endsection



@section('bodyclass')
    class="to-top  on-side-way"
@endsection

@section('content')
    

        <style>
            #oem{
                color: #000;
                font-weight: 500;
            }

            
            
            @media (max-width: 575.98px) {
                .slider-text-container p, .year-val ,.year-info , .readmore-content{
                    margin-top: 0;
                    font-size: 70%;
                    line-height: 18px;
                }
                .slider-text-container h1{
                    font-size:1em;
                }
                .slider-text-container h4 , .readmore-head{
                    font-size:0.8em;
                    line-height: 16px;
                    margin-bottom: 0px;
                    padding: 0px 0px 0px 0px;
                }
                .slider-text-container p .btn-internal{
                    font-size: 80%;
                }
                .main-header .main-header-inner img{
                    max-width: 200px;
                }
            }

            @media (min-width: 576px) and (max-width: 767.98px) {
                .slider-text-container p, .year-val ,.year-info , .readmore-content{
                    margin-top: 0;
                    font-size: 70%;
                    line-height: 18px;
                }
                .slider-text-container h1{
                    font-size:1em;
                }
                .slider-text-container h4 , .readmore-head{
                    font-size:0.8em;
                    line-height: 16px;
                    margin-bottom: 0px;
                    padding: 0px 0px 0px 0px;
                }
                .slider-text-container p .btn-internal{
                    font-size: 80%;
                }
                .main-header .main-header-inner img{
                    max-width: 200px;
                }
            }

            @media (min-width: 768px) and (max-width: 991.98px) {
                .slider-text-container p, .year-val ,.year-info , .readmore-content{
                    margin-top: 0;
                    font-size: 70%;
                    line-height: 18px;
                }
                .slider-text-container h1{
                    font-size:1.2em;
                }
                .slider-text-container h4 , .readmore-head{
                    font-size:1em;
                    line-height: 18px;
                    margin-bottom: 0px;
                    padding: 0px 0px 0px 0px;
                }
                .slider-text-container p .btn-internal{
                    font-size: 80%;
                }
                .main-header .main-header-inner img{
                    max-width: 200px;
                }
            }

            @media (min-width: 992px) and (max-width: 1299.98px) {
                .slider-text-container p, .fourpillarsp{
                    margin-top: 0;
                    font-size: 80%;
                    line-height: 18px;
                }
                .slider-text-container h1{
                    font-size:1.2em;
                }
                .slider-text-container h4 , .readmore-head{
                    font-size:1em;
                    line-height: 18px;
                    margin-bottom: 0px;
                    padding: 0px 0px 0px 0px;
                }
                .slider-text-container p .btn-internal{
                    font-size: 80%;
                }
                .main-header .main-header-inner img{
                    max-width: 200px;
                }
            }

            @media (min-width: 1300px) and (max-width: 1599.98px) {
                .slider-text-container p , .year-val ,.year-info , .readmore-content{
                    margin-top: 0;
                    font-size: 80%;
                    line-height: 18px;
                }
                .navbar-nav{
                    font-size: 80%;
                }
                .slider-text-container h1 {
                    font-size:1.3em;

                    
                }
                .slider-text-container h4 , .readmore-head{
                    font-size:1em;
                    line-height: 18px;
                    margin-bottom: 0px;
                    padding: 0px 0px 0px 0px;
                }
                .slider-text-container p .btn-internal{
                    font-size: 80%;
                }
                .main-header .main-header-inner img{
                    max-width: 200px;
                }
            }
            h1,h4{
                color:#211651;
            }

            @media only screen and (max-width: 600px) 
            {


                .fixed-header{
                    position: fixed;        
                }
            }
            
            @media only screen and (max-width: 600px) 
            {


                .headercolor{
                    background-color: rgb(255, 255, 255);
                    padding-top: -10px;
                    padding-bottom: 63px;
                    z-index: 1;
                            
                }
            }
        </style>
        <div class="container-fluid">
            <div class="row headercolor">
                <!--side-panel-->
                <div class="col-lg-2 p-0 main-side-panel fixed-header">
                    @include('partials.search')

                    <nav class="navbar navbar-expand-lg navbar-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <!--<span class="navbar-toggler-icon"></span>-->
                            <div id="nav-icon" class="nav-icon">
                                <div class="nav-icon-inner">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </button>

                        @include('partials.sidebar')
                    </nav>
                </div>

                <script>
                    $(document).ready(function(){
                        $('#nav-icon').click(function(){
                            $(this).toggleClass('open');
                        });
                    });
                </script>
                <!--side-panel-->

                <div class="col-lg-10 p-0">
                    <!--header-->
                        <div class="main-header headercolor">
                            <div class="main-header-inner">
                                <a href="{{ route('welcome')}}"><img src="{{ asset('site/img/dreamron.png?v=1.000.000.012') }}" alt="Dreamron"></a>
                            </div>
                        </div>
                    <!--header-->

                    <div class="dreamron-common dragging-containerx d-none d-sm-block">
                        <div class="hideMe">
                            <svg id="downArrow" class="arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
                                <g stroke-linejoin="round" stroke-linecap="round">
                                    <circle r="46" cx="50" cy="50"/>
                                    <polyline points="25 40, 50 70, 75 40"></polyline>
                                </g>
                            </svg>
                            <svg id="upArrow" class="arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
                                <g stroke-linejoin="round" stroke-linecap="round">
                                    <circle r="46" cx="50" cy="50"/>
                                    <polyline points="25 60, 50 30, 75 60"></polyline>
                                </g>
                            </svg>
                            <div id="masterWrap">
                                <div id="panelWrap">
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                        <div class="tns-item slider-text-item-container">
                                            <div class="slider-text-container">
                                                <div class="text-container top-up">
                                                    <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <span>Contract Manufacturing </span></div>
                                                    
                                                    <h4>Contract Manufacturing  
                                                    </h4>
                                                    <p>
                                                        Harumi Holdings and Kindai Kagaku Lanka (Pvt.) Limited, also undertakes contract manufacturing projects from local and international firms who are looking for manufacturing as a service to get their cosmetic related products manufactured by a reputed and certified manufacturing company. 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item dreamron-oem1">

                                            </div>
                                        </div>
                                    </section>
                                    
                                    <section class="row m-0">
                                        <div class="col-md-6 col-lg-5 p-0 tns-outer dreamron-text-slider">
                                        <div class="tns-item slider-text-item-container">
                                            <div class="slider-text-container">
                                                <div class="text-container top-up">
                                                    <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <span>Contract Manufacturing </span></div>
                                                    
                                                    <h4>Contract Manufacturing  
                                                    </h4>
                                                    <p>
                                                        With over 20 years industrial experience, Dreamron is the leading cosmetic manufacturing company in Sri Lanka that undertakes contract manufacturing products from Island’s leading Multinational & local companies and large cosmetic brands owners in the Asian continent. 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7 p-0 tns-outer dreamron-main-slider">
                                            <div class="dreamron-item dreamron-oem2">

                                            </div>
                                        </div>
                                    </section>

                                </div>
                            </div>
                            <div class="dots">
                            </div>
                            <div class="toolTips">
                                <div class="toolTip">
                                    Contract Manufacturing 
                                </div>
                                <div class="toolTip">
                                    Contract Manufacturing 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-block d-sm-none">
                        <section class="row m-0 dreamron-item dreamron-oem1">
                            <div class="col-md-6 col-lg-5 p-0 tns-outer"  style="background-color: aliceblue; opacity:0.9">
                            <div class="tns-item slider-text-item-container">
                                <div class="slider-text-container">
                                    <div class="text-container top-up">
                                        <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <span>Contract Manufacturing </span></div>
                                        
                                        <h4>Contract Manufacturing  
                                        </h4>
                                        <p>
                                            Harumi Holdings and Kindai Kagaku Lanka (Pvt.) Limited, also undertakes contract manufacturing projects from local and international firms who are looking for manufacturing as a service to get their cosmetic related products manufactured by a reputed and certified manufacturing company. 
                                        </p>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </section>
                        
                        <section class="row m-0 dreamron-item dreamron-oem2">
                            <div class="col-md-6 col-lg-5 p-0 tns-outer"  style="background-color: aliceblue; opacity:0.9">
                            <div class="tns-item slider-text-item-container">
                                <div class="slider-text-container">
                                    <div class="text-container top-up">
                                        <div class="bred-crumb"><span>Our Story</span> <i class="fa fa-arrow-right"></i> <span>Contract Manufacturing </span></div>
                                        
                                        <h4>Contract Manufacturing  
                                        </h4>
                                        <p>
                                            With over 20 years industrial experience, Dreamron is the leading cosmetic manufacturing company in Sri Lanka that undertakes contract manufacturing products from Island’s leading Multinational & local companies and large cosmetic brands owners in the Asian continent. 
                                        </p>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>

        @include('partials.footer')
@endsection


@section('script')
    <script src="{{ asset('site/js/stopexecutionontimeout.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/gsap-latest-beta.min.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/draggable3.min.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/inta.mini.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/scroll-slider.js?v=1.000.000.012') }}"></script>
@endsection