@extends('layouts.site')

@section('title')
    <title>Home - Dreamron</title>
@endsection



@section('bodyclass')
    id="expander"  class="home on-side-way to-top"
@endsection
  
@section('content')
<!--header-->
<!--header-->
    <style>
        #home {
            color: #000;
            font-weight: 500;
        }
        
            

            
    </style>


<div class="container-fluid main-expander ">
        <div class="row">
            <!--side-panel-->     


            <div class="col-lg-2 p-0 main-side-panel">
                @include('partials.search')

                <nav class="navbar navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <!--<span class="navbar-toggler-icon"></span>-->
                        <div id="nav-icon" class="nav-icon">
                            <div class="nav-icon-inner">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </button>
            
                    @include('partials.sidebar')
                </nav>
            </div>      



            <script>
                $(document).ready(function(){
                    $('#nav-icon').click(function(){
                        $(this).toggleClass('open');
                    });
                });
                
            </script>
            <!--side-panel-->

            <div class="col-lg-10">
                
                <div class="main-header">
                    <div class="main-header-inner text-center">
                        <a href="{{ route('welcome')}}"><img src="{{ asset('site/img/dreamron.png?v=1.000.000.015') }}"  alt="Dreamron"></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 p-0 tns-outer dreamron-text-slider">
                        <div class="tns-ovh">
                            <div class="tns-inner">
                                <div class="vertical tns-slider tns-carousel tns-subpixel tns-calc tns-vertical"
                                     id="vertical">
                                    <div class="item tns-item tns-slide-cloned slider-text-item-container"
                                         aria-hidden="true" tabindex="-1">
                                        <div class="slider-text-container">
                                            <div class="text-container">
                                                <!--<h1 class="cap-me">over <span class="highlight">250</span> hair-care and skin-care <span class="highlight">cosmetic</span> products to make you live <span class="highlight">beautifully</span>-->
                                                <!--</h1>-->
                                                <h1 class="cap-me">over <span class="highlight">250 Cosmetic</span>, Hair Care and Skin Care products for  <span class="highlight">A World Class You</span>
                                                </h1>
                                            </div>
                                        </div>
                                    </div>
                                    <!--
                                        <div class="item tns-item tns-slide-cloned slider-text-item-container"
                                            aria-hidden="true" tabindex="-1">
                                            <div class="slider-text-container">
                                                <div class="text-container">
                                                    <h1>The Thing of Beauty <br>
                                                        is a Joy Forever
                                                    </h1>
                                                </div>
                                            </div>
                                        </div>
                                    -->
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-8 p-0 tns-outer dreamron-main-slider">
                        <div class="tns-ovh">
                            <div class="tns-inner">
                                <div class="vertical tns-slider tns-carousel tns-subpixel tns-calc tns-vertical"
                                    id="vertical-text">
                                    <div class="item tns-item tns-slide-cloned dreamron-item dreamron-hp1 cover"
                                        aria-hidden="true" tabindex="-1">
                                        <!--
                                        <h5 class="image-text-content"  >Over 250 Hair-care and Skin-care Cosmetic Products to make you live beautifully </h5>
                                        
                                        -->
                                    </div>
                                        <!--
                                        <div class="item tns-item tns-slide-cloned dreamron-item dreamron-hp2 cover"
                                            tabindex="-1">
                                            <h5 class="image-text-content" >Fully Accredited and South-Asia’s largest beauty college academy </h5>
                                        </div>
                                        <div class="item tns-item tns-slide-cloned dreamron-item dreamron-hp3 cover"
                                            tabindex="-1">
                                            <h5 class="image-text-content" >Sri Lankas largest Cosmetic Product manufacturer since year 1998 with over 120,000 Square Foot premises</h5>
                                        </div>
                                -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--footer-->
    <div class="main-footer">
        
        
        
        <ul class="social-icons">
            <li>
                <a href="https://www.facebook.com/dreamronsrilanka/" target="_blank">
                    <i class="fa fa-facebook"></i>
                </a>
            </li>
            <li>
                <a href="https://www.instagram.com/dreamronsrilanka" target="_blank">
                    <i class="fa fa-instagram"></i>
                </a>
            </li>
            <li>
                <a href="#" target="_blank">
                    <i class="fa fa-twitter"></i>
                </a>
            </li>
        </ul>
    </div>
    <!--/.footer-->
@endsection

@section('script')
    

    

    
<script>
    var slider = tns({
        "container": "#vertical",
        "items": 1,
        "axis": "vertical",
        "speed": 1700,
        "autoplay": true
    });
</script>
<script>
    var slider = tns({
        "container": "#vertical-text",
        "items": 1,
        "axis": "vertical",
        "speed": 1100,
        "autoplay": true
    });
</script>
    <!-- Popper.js, then Bootstrap JS-->
    <script src="{{ asset('site/js/vendors.js?v=1.000.000.015') }}" crossorigin="anonymous"></script>
    <!-- Custom js calling index.js-->
    <script src="{{ asset('site/js/index.js?v=1.000.000.015') }}" crossorigin="anonymous"></script>
    
    
    <!--/.foot-->
@endsection

