@extends('layouts.site')

@section('title')
    <title>Contact - Dreamron</title>
@endsection

@section('bodyclass')
    class="to-top contact "
@endsection

@section('content')

    <style>
        #contact {
            color: #000;
            font-weight: 500;
        }
        
    </style>
    <div class="container-fluid">
        <div class="row">
            <!--side-panel-->
            <div class="col-lg-2 p-0 main-side-panel">
                @include('partials.search')

                <nav class="navbar navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <!--<span class="navbar-toggler-icon"></span>-->
                        <div id="nav-icon" class="nav-icon">
                            <div class="nav-icon-inner">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </button>

                    @include('partials.sidebar')
                </nav>
            </div>

            <script>
                $(document).ready(function(){
                    $('#nav-icon').click(function(){
                        $(this).toggleClass('open');
                    });
                });
            </script>
            <!--side-panel-->
            <!--<h3>How Can We Help You Today</h3>-->


            <div class="col-lg-10">
            <!--header-->
            <div class="main-header">
                <div class="main-header-inner">
                    <a href="{{ route('welcome')}}"><img src="{{ asset('site/img/dreamron.png?v=1.000.000.012') }}" alt="Dreamron"></a>
                </div>
            </div>
            <!--header-->
                <div class="row main-area">
                    <div class="col-lg-6 col-xl-6 left-area">
                        <div class="email-form-container d-lg-none">
                            <button class="navbar-toggler">
                                <div class="nav-icon open">
                                    <div class="nav-icon-inner">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </div>
                            </button>
                            <form action="{{ route('contactmail') }}" method="post">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="">First name</label>
                                            <input type="text" class="form-control" id="1" name="first_name"  placeholder="First name">
                                            <small id="" class="form-text text-muted">We'll never share your email with anyone
                                                else.
                                            </small>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="">Last name</label>
                                            <input type="text" class="form-control" id="2"  name="last_name" placeholder="Last name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="email" class="form-control" id="3" name="email"  placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="">Telephone</label>
                                            <input type="text" class="form-control" id="4" name="telephone" placeholder="Telephone">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Subject</label>
                                            <input type="text" class="form-control" id="5" name="subject" placeholder="Subject">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Message</label>
                                            <textarea class="form-control" id="6"  name="message" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Sent <i class="fa fa-send"></i></button>
                            </form>
                        </div>
                        <div class="sent-email">
                            <span class="">Send your message<i class="rocket fa fa-send"></i></span>
                            <div class="cover-me"></div>
                            
                            @if (isset($details))
                                <p>Your message was sented.</p>
                            @endif
                        </div>
                        <div class="row contact-areas">
                            <div class="col-12 col-sm-4 col-md-6 ">
                                <h6 class="contact-area-name">Kindai Kagaku Lanka (Pvt) Ltd</h6>
                                <ul class="a-contact-section">
                                    <li>
                                        <a>
                                            Poruwadanda, Horana, Sri Lanka

                                        </a>
                                    </li>
                                    <li>
                                        <a href="mailto:marketing@kindai.lk">
                                            marketing@kindai.lk</a>
                                    </li>
                                    <li>
                                        <a href="mailto:info@kindai.lk">
                                            info@kindai.lk</a>
                                    </li>
                                    <li>
                                        <a href="tel:(+94) 344 941 9144">
                                            (+94) 344 941 914</a>
                                    </li>
                                    <li>
                                        <a href="tel:(+94) 342 255 628">
                                            Fax: (+94) 342 255 628</a>
                                    </li>
                                    <li>
                                        <a href="tel:(+94) 714 291 870">
                                            International Business Inquiries: (+94) 714 291 870</a>
                                    </li>
                                    <li>
                                        <a href="tel:(+94) 714 444 700">
                                            Hot line: (+94) 714 444 700</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-4 col-md-6">
                                <h6 class="contact-area-name">Dreamron Lanka (Pvt) Ltd</h6>
                                <ul class="a-contact-section">
                                    <li>
                                        <a>
                                            No 112, Sunethradevi Road, Kohuwala,<br>
                                            Nugegoda, Sri Lanka
                                        </a>
                                    </li>
                                    <li>
                                        <a href="mailto:info@dreamroncosmetics.com">
                                            info@dreamroncosmetics.com</a>
                                    </li>
                                    <li>
                                        <a href="tel:(+94) 112 853804">
                                            (+94) 112 853804</a>
                                    </li>
                                    <li>
                                        <a href="tel:(+94) 112 853805">
                                            Fax: (+94) 112 853805</a>
                                    </li>
                                    <li>
                                        <a href="tel:(+94) 112 853804">
                                            (+94) 112 853804</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-4 col-md-6">
                                <h6 class="contact-area-name">Harumi Holdings Lanka (Pvt) Ltd</h6>
                                <ul class="a-contact-section">
                                    <li>
                                        <a>
                                            Poruwadanda, Horana, Sri Lanka
                                        </a>
                                    </li>
                                    <li>
                                        <a href="mailto:info@harumi.com">
                                            info@harumi.com</a>
                                    </li>
                                    <li>
                                        <a href="tel:(+94) 344 941 514">
                                            (+94) 344 941 514</a>
                                    </li>
                                    <li>
                                        <a href="tel:(+94) 344 941 514">
                                            Fax: (+94) 344 941 514</a>
                                    </li>
                                    <li>
                                        <a href="tel:(+94) 714 444 700">
                                            Hot line: (+94) 714 444 700</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-4 col-md-6">
                                <h6 class="contact-area-name">Dreamron College of Art and Beauty</h6>
                                <ul class="a-contact-section">
                                    <li>
                                        <a>
                                            No 221, High Level Road, Maharagama
                                        </a>
                                    </li>
                                    <li>
                                        <a href="mailto:info@dreamronbeautycollege.com">
                                            info@dreamronbeautycollege.com</a>
                                    </li>
                                    <li>
                                        <a href="tel:(+94) 714 444 750">
                                            (+94) 714 444 750</a>
                                    </li>
                                    <li>
                                        <a href="tel:(+94) 711 000 503">
                                            (+94) 711 000 503</a>
                                    </li>
                                    <li>
                                        <a href="tel:(+94) 714 444 700">
                                            Hot line: (+94) 714 444 700</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d63382.72215962306!2d79.931283!3d6.8401290999999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2slk!4v1606235060224!5m2!1sen!2slk"
                                width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""
                                aria-hidden="false" tabindex="0"></iframe>
                    </div>
                    <div class="col-lg-6 col-xl-6 right-area">
                        <div class="email-form-container">
                            <button class="navbar-toggler">
                                <div class="nav-icon open">
                                    <div class="nav-icon-inner">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </div>
                            </button>
                            <form action="{{ route('contactmail') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="">First name</label>
                                            <input type="text" class="form-control" id="1" name="first_name" placeholder="First name">
                                            <small id="" class="form-text text-muted">We'll never share your email with anyone
                                                else.
                                            </small>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="">Last name</label>
                                            <input type="text" class="form-control" id="2"  name="last_name" placeholder="Last name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                    <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="email" class="form-control" id="3"  name="email" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                    <div class="form-group">
                                            <label for="">Telephone</label>
                                            <input type="text" class="form-control" id="4"  name="telephone" placeholder="Telephone">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Subject</label>
                                            <input type="text" class="form-control" id="5"  name="subject" placeholder="Subject">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Message</label>
                                            <textarea class="form-control" id="6"  name="message" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Sent <i class="fa fa-send"></i></button>
                            </form>
                        </div>
                        <div class="map-content">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d63382.72215962306!2d79.931283!3d6.8401290999999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2slk!4v1606235060224!5m2!1sen!2slk"
                                    width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""
                                    aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('partials.footer')
@endsection

@section('script')
    
    <script src="{{ asset('site/js/stopexecutionontimeout.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/gsap-latest-beta.min.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/draggable3.min.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/inta.mini.js?v=1.000.000.012') }}"></script>
    <script src="{{ asset('site/js/scroll-slider.js?v=1.000.000.012') }}"></script>

    <!--/.foot-->
    <script>
        $(document).ready(function () {
            $('.sent-email').click(function () {
                $(".email-form-container").toggleClass("open-form");
                $(".main-footer").toggleClass("open-form");
                $("iframe").toggleClass("open-form");
                $(".main-side-panel").addClass("open-form");
                $(".left-area").addClass("open-form");
            });
            $('.nav-icon').click(function () {
                $(".email-form-container").removeClass("open-form");
                $(".main-side-panel").removeClass("open-form");
                $(".left-area").removeClass("open-form");
                $("iframe").removeClass("open-form");
            });
        });
    </script>

    <style>
        .test {
            background-color: yellow;
        }
    </style>
@endsection