<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    
    <!--head-->
<head>
    <meta charset="UTF-8">
    @yield('title')
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--Favicon -->
    <link rel="shortcut icon" type='image/x-icon' href="{{ asset('site/img/favicon/favicon.ico?v=1.000.000.012') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('site/img/favicon/apple-touch-icon.png?v=1.000.000.012') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('site/img/favicon/favicon-32x32.png?v=1.000.000.012') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('site/img/favicon/favicon-16x16.png?v=1.000.000.012') }}">
    <link rel="manifest" href="{{ asset('site/img/favicon/site.webmanifest?v=1.000.000.012') }}">
    <link rel="mask-icon" href="{{ asset('site/img/favicon/safari-pinned-tab.svg?v=1.000.000.012') }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="{{ asset('site/css/tiny-slider.css?v=1.000.000.012') }}">
    <!--[if (lt IE 9)]><script src="js/tiny-slider.helper.ie8.js"></script><![endif]-->
    <script src="{{ asset('site/js/tiny-slider.js') }}"></script>

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('site/css/bootstrap.css?v=1.000.000.012') }}" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('site/css/aos.css?v=1.000.000.012') }}" crossorigin="anonymous">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{ asset('site/css/fontawesome.css?v=1.000.000.012') }}" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('site/css/style.css?v=1.000.000.012') }}" crossorigin="anonymous">

    <!-- jQuery JS-->
    <script src="{{ asset('site/js/jquery.js?v=1.000.000.012') }}" crossorigin="anonymous"></script>

    @yield('css')
</head>
<!--/.head-->

    
<body @yield('bodyclass')>

    @yield('content')

    
    <!--foot-->
    <!-- Popper.js, then Bootstrap JS-->
    <script src="{{ asset('site/js/vendors.js?v=1.000.000.012') }}" crossorigin="anonymous"></script>
    <!-- Custom js calling index.js-->
    <script src="{{ asset('site/js/index.js?v=1.000.000.012') }}" crossorigin="anonymous"></script>

    @yield('script')

</body>
</html>


