

<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="A powerful and conceptual apps base dashboard template that especially build for developers and programmers.">

    <!-- Fav Icon  -->
    <link rel="shortcut icon" type='image/x-icon' href="{{ asset('site/img/favicon/favicon.ico?v=1.000.000.012') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('site/img/favicon/apple-touch-icon.png?v=1.000.000.012') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('site/img/favicon/favicon-32x32.png?v=1.000.000.012') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('site/img/favicon/favicon-16x16.png?v=1.000.000.012') }}">
    <link rel="manifest" href="{{ asset('site/img/favicon/site.webmanifest?v=1.000.000.012') }}">
    <link rel="mask-icon" href="{{ asset('site/img/favicon/safari-pinned-tab.svg?v=1.000.000.012') }}" color="#5bbad5">
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="{{ asset('admin/css/dashlite.css?ver=2.0.0')}}">
    <link id="skin-default" rel="stylesheet" href="{{ asset('admin/css/theme.css?ver=2.0.0')}}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>The Dreamron Dashboard</title>

    @yield('css')
</head>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <div class="nk-sidebar nk-sidebar-fixed is-dark " data-content="sidebarMenu">
                <div class="nk-sidebar-element nk-sidebar-head">
                    <div class="nk-sidebar-brand">
                        <a href="{{ route('welcome') }}" class="logo-link">
                            <img class="logo-light logo-img" src="{{ asset('images/logo.png') }}" alt="logoQ">
                            <img class="logo-dark logo-img" src="{{ asset('images/logo-dark-small.png') }}"  alt="logo-dark">
                        </a>
                    </div>
                    <div class="nk-menu-trigger mr-n2">
                        <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
                    </div>
                </div><!-- .nk-sidebar-element -->
                <div class="nk-sidebar-element">
                    <div class="nk-sidebar-content">
                        <div class="nk-sidebar-menu" data-simplebar>
                            <ul class="nk-menu">
                                <li class="nk-menu-heading">
                                    <h6 class="overline-title text-primary-alt">DashBoard</h6>
                                </li><!-- .nk-menu-heading -->
                                
                                <li class="nk-menu-item">
                                    <a href="" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="icon ni ni-menu-squared"></em></span>
                                        <span class="nk-menu-text">Dreamron DashBoard</span>
                                    </a>
                                </li><!-- .nk-menu-item -->
                                <li class="nk-menu-heading">
                                    <h6 class="overline-title text-primary-alt">Main Branches</h6>
                                </li><!-- .nk-menu-heading -->
                                
                                <li class="nk-menu-item">
                                    <a href="{{ route('products.index') }}" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="icon ni ni-dashlite"></em></span>
                                        <span class="nk-menu-text">Products Manage</span>
                                    </a>
                                </li><!-- .nk-menu-item -->
                                <li class="nk-menu-item">
                                    <a href="{{ route('brands.index') }}" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="icon ni ni-list-round"></em></span>
                                        <span class="nk-menu-text">Brands Manage</span>
                                    </a>
                                </li><!-- .nk-menu-item -->
                                <li class="nk-menu-item">
                                    <a href="{{ route('categories.index') }}" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="icon ni ni-tag"></em></span>
                                        <span class="nk-menu-text">Categories Manage</span>
                                    </a>
                                </li><!-- .nk-menu-item -->
                                <li class="nk-menu-item">
                                    <a href="{{ route('subcategories.index') }}" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="icon ni ni-tag"></em></span>
                                        <span class="nk-menu-text">Sub Categories Manage</span>
                                    </a>
                                </li><!-- .nk-menu-item -->
                                <li class="nk-menu-item">
                                    <a href="{{ route('blog.index') }}" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="icon ni ni-tag"></em></span>
                                        <span class="nk-menu-text">Blogs Manage</span>
                                    </a>
                                </li><!-- .nk-menu-item -->
                                @if (auth()->user()->isAdmin())
                                <li class="nk-menu-heading">
                                    <h6 class="overline-title text-primary-alt">System</h6>
                                </li><!-- .nk-menu-heading -->
                                
                                <li class="nk-menu-item">
                                    <a href="" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="icon ni ni-users"></em></span>
                                        <span class="nk-menu-text">Update Password</span>
                                    </a>
                                </li><!-- .nk-menu-item -->
                                @endif
                            </ul><!-- .nk-menu -->
                        </div><!-- .nk-sidebar-menu -->
                    </div><!-- .nk-sidebar-content -->
                </div><!-- .nk-sidebar-element -->
            </div>
            <!-- sidebar @e -->
            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="html/index.html" class="logo-link">
                                    <img class="logo-light logo-img" src="{{ asset('images/logo.png')}}" srcset="./images/logo2x.png 2x" alt="logoO">
                                    <img class="logo-dark logo-img" src="{{ asset('images/logo-dark.png')}}" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <img src="{{ Gravatar::src(auth()->user()->email) }}" >
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">{{ auth()->user()->role }}</div>
                                                    <div class="user-name dropdown-indicator">{{ auth()->user()->name }}</div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <img src="{{ Gravatar::src(auth()->user()->email) }}" >
                                                    </div>
                                                    <div class="user-info">
                                                        <span class="lead-text">{{ auth()->user()->name }}</span>
                                                        <span class="sub-text">{{ auth()->user()->email }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">                                                
                                                <ul class="link-list">
                                                    <li>
                                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                                        onclick="event.preventDefault();
                                                                    document.getElementById('logout-form').submit();">
                                                        <em class="icon ni ni-signout"></em><span>{{ __('Logout') }}
                                                        </span></a>
                
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="components-preview wide-lg mx-auto " id="lokuwenna">
                                
                                @yield('content')

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="{{ asset('admin/js/bundle.js?ver=2.0.0')}}"></script>
    <script src="{{ asset('admin/js/scripts.js?ver=2.0.0')}}"></script>
    @yield('script')
</body>

</html>
