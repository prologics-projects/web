@extends('layouts.site')

@section('title')
    <title>Home - Dreamron</title>
@endsection

@section('bodyclass')
    id="expander" class="home on-side-way"
@endsection
  
@section('css')


@endsection

@section('content')
    <!--header-->
    <div class="main-header">
        <div class="main-header-inner">
            <a href="index.html"><img src="{{ asset('site/img/dreamron.png?v=1.000.000.012') }}" alt="Dreamron"></a>
        </div>
    </div>
    <!--header-->
    <style>
        #home {
            color: #000;
            font-weight: 500;
        }

        @media screen and  (max-width:700px){
            .large{
                display: none;
            }
        }
        @media screen and  (min-width:700px){
            .small{
                display: none;
            }
        }

    </style>

    @if ($launch->status==0)
        <div class="launch" id="launch-section" >
            <video autoplay loop class="video-background" plays-inline id="launch-video">
              <source src="{{ asset('site/img/launch.mp4') }}" type="video/mp4">
            </video>
            <h1 class="launch-text launched" style="font-size: 120px;">Ready To Live</h1>
            <h1 id="demo" class="launch-text-count" style="font-size: 140px;"></h1>
            <button id="launch-btn"  class="btn btn-outline-danger btn-lg">LAUNCH</button>
        </div>
    @endif
    <!--video-->
    
    <div class="main-video " >
        <video id="bg-video" class="large">
            <source src="{{ asset('site/videos/1080_1920.mp4?v=1.000.000.012') }}" type="video/mp4">
            <source src="{{ asset('site/videos/1080_1920.ogg?v=1.000.000.012') }}" type="video/mp4">
            Your browser does not support HTML5 video -large.
        </video>
        <video id="bg-video" class="small">
            <source src="{{ asset('site/videos/MOBILE.mp4?v=1.000.000.012') }}" type="video/mp4">
            <source src="{{ asset('site/videos/MOBILE.ogg?v=1.000.000.012') }}" type="video/mp4">
            Your browser does not support HTML5 video -small.
        </video>
        <p id="demo"></p>

        <!--<button class="fa fa-angle-up go go-up" onclick="scrollToTop()"></button>-->
        <div class="go-up" onclick="location.href = '{{ route('homepage')}}'" style="display: block;">
            <div class="mouse" onclick="location.href = '{{ route('homepage')}}'">
                <div class="wheel"></div>
                <div class="dot-container"><span class="unu"></span> <span class="doi"></span> <span class="trei"></span></div>
            </div>
        </div>
        <i class="volume-control fa fa-volume-up" id="muteOrNot" onclick="muteOrNot()" type="button"></i>
        <i class="video-control fa fa-pause-circle" id="playPauseBtn" onclick="playPauseController()" type="button"></i>
    </div>
    <!--/.video-->


@endsection

@section('script')
    
    <!--foot-->
    <script>
        document.getElementById('bg-video').play();
    </script>

    <script>
        var slider = tns({
            "container": "#vertical",
            "items": 1,
            "axis": "vertical",
            "speed": 1700,
            "autoplay": true
        });
    </script>
    <script>
        var slider = tns({
            "container": "#vertical-text",
            "items": 1,
            "axis": "vertical",
            "speed": 1100,
            "autoplay": true
        });
    </script>

    

    
    <!--foot-->
    <!-- Popper.js, then Bootstrap JS-->
    <script src="{{ asset('site/js/vendors.js?v=1.000.000.012') }}" crossorigin="anonymous"></script>
    <!-- Custom js calling index.js-->
    <script src="{{ asset('site/js/index.js?v=1.000.000.012') }}" crossorigin="anonymous"></script>
    
    <script>
    </script>
    
    <!--/.foot-->
@endsection

