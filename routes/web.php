<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','SiteController@index')->name('welcome');
Route::get('/homepage','SiteController@homepage')->name('homepage');
Route::get('/product','SiteController@product')->name('product');
Route::get('/rnd','SiteController@rnd')->name('rnd');
Route::get('/oem','SiteController@oem')->name('oem');
Route::get('/contact','SiteController@contact')->name('contact');
Route::get('/story','SiteController@story')->name('story');
Route::get('/blogs','SiteController@blog')->name('blogs');
Route::get('/galleryshow','SiteController@galleryshow')->name('galleryshow');
Route::get('/albumshow/{album}','SiteController@albumshow')->name('albumshow');
Route::post('/contactmail','SiteController@contactmail')->name('contactmail');
Route::get('/loadproducts','SiteController@loadproducts')->name('loadproducts');
Route::post('/search','SiteController@search')->name('search');
Route::get('/thisyear','SiteController@thisyear')->name('thisyear');
Route::get('/pastyear','SiteController@pastyear')->name('pastyear');
Route::get('/nextyear','SiteController@nextyear')->name('nextyear');
Route::get('/jan/{blog}','SiteController@jan')->name('jan');
Route::get('/feb/{blog}','SiteController@feb')->name('feb');
Route::get('/mar/{blog}','SiteController@mar')->name('mar');
Route::get('/apr/{blog}','SiteController@apr')->name('apr');
Route::get('/may/{blog}','SiteController@may')->name('may');
Route::get('/jun/{blog}','SiteController@jun')->name('jun');
Route::get('/jul/{blog}','SiteController@jul')->name('jul');
Route::get('/aug/{blog}','SiteController@aug')->name('aug');
Route::get('/sep/{blog}','SiteController@sep')->name('sep');
Route::get('/oct/{blog}','SiteController@oct')->name('oct');
Route::get('/nov/{blog}','SiteController@nov')->name('nov');
Route::get('/dec/{blog}','SiteController@dec')->name('dec');
Route::get('/view/{blog}','SiteController@view')->name('view');

Route::get('/launched','SiteController@launched')->name('launched');

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('blog', 'BlogController');
    Route::resource('brands', 'BrandsController');
    Route::resource('categories', 'CategoriesController'); 
    Route::resource('subcategories', 'SubCategoriesController'); 
    Route::resource('products', 'ProductController'); 
    Route::get('getSubCat/{id}', 'ProductController@GetSubCat');
    Route::put('productimage', 'ProductController@productimage')->name('products.productimage'); 
    Route::get('productimageset/{product}', 'ProductController@productimageset')->name('products.productimageset');
    Route::post('productstatus/{product}', 'ProductController@status')->name('products.status'); 
    Route::get('passwordreset', 'UsersController@passwordreset')->name('passwordreset'); 
    Route::get('changepwd/{user}', 'UsersController@changepwd')->name('changepwd'); 
    Route::get('ckeditor', 'CkeditorController@index');
    Route::post('ckeditor/upload', 'CkeditorController@upload')->name('ckeditor.upload');   

    Route::get('/album','AlbumController@index')->name('album.index');
    Route::get('/album-getdata','AlbumController@getdata')->name('album.getdata');
    Route::post('/album/add','AlbumController@add')->name('album.add');
    Route::post('/album/edit','AlbumController@edit')->name('album.edit');
    Route::post('/album/delete','AlbumController@delete')->name('album.delete');

    Route::get('album-images/{album}','GalleryController@images')->name('album.images');
    Route::post('dropzone/upload', 'GalleryController@upload')->name('dropzone.upload');
    Route::get('dropzone/fetch', 'GalleryController@fetch')->name('dropzone.fetch');
    Route::get('dropzone/delete', 'GalleryController@delete')->name('dropzone.delete');
    Route::get('getfile/{id}', 'GalleryController@getfile')->name('getfile');

});
