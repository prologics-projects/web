<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\Category;
use App\SubCategory;
use App\Product;
use App\Http\Requests\Product\ProductsCreateController;
use App\Http\Requests\Product\ProductsUpdateController;
use DB;
use Image;

class ProductController extends Controller
{
    public function GetSubCat($id){
        echo json_encode(DB::table('sub_categories')->where('category_id', $id)->get());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products.index')->with('products',Product::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create')
        ->with('brands',Brand::all())
        ->with('categories',Category::all())
        ->with('subcategories',SubCategory::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductsCreateController $request)
    {

        $product=Product::create([
            'category_id'=>$request->category_id,
            'subcategory_id'=>$request->subcategory_id,
            'brand_id'=>$request->brand_id,
            'name'=>$request->name,
            'sku'=>$request->sku,
            'description'=>$request->description,
            'status'=>1,
            'amount'=>$request->amount,
            'url'=>$request->url,
            'barcode'=>$request->barcode,
        ]);

        session()->flash('success','Product Created Successfully');
        return redirect(route('products.productimageset',$product->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('products.create')
        ->with('product',$product)
        ->with('brands',Brand::all())
        ->with('categories',Category::all())
        ->with('subcategories',SubCategory::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductsUpdateController $request, Product $product)
    {
        $data=$request->only(['category_id','subcategory_id','brand_id','name','url','barcode','sku','description','status','amount']);

        $product->update($data);

        session()->flash('success','Product Updated Successfully');
        return redirect(route('products.productimageset',$product->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->deleteImage();
        $product->delete();

        session()->flash('success','Product Deleted Successfully');
        return redirect(route('products.index'));

    }

    public function productimageset(Product $product){
        return view('products.image')
        ->with('product',$product);
    }

    public function productimage(Request $request){
        //get filename with extension
        $filenamewithextension = $request->file('image')->getClientOriginalName();

        //get filename without extension
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
    
        //get file extension
        $extension = $request->file('image')->getClientOriginalExtension();
    
        //filename to store
        $filenametostore = $filename.'_'.time().'.'.$extension;
    
        $img = Image::make($request->file('image'));
        $croppath = 'storage/products/'.$filenametostore;
        $croppath_save = 'products/'.$filenametostore;
        if(!is_null($request->input('w'))){
            $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'))->resize(300, 300);    
        }
        $img->save($croppath);


        $data['image']=$croppath_save;
        $product=Product::find($request->id);
        $product->deleteImage();
        $product->update($data);

        session()->flash('success','Product Updated');
        return redirect(route('products.index'));

    }

    public function status(Product $product){
        if($product->status=='1'){            
            $product->status='0';
            $product->save();
            session()->flash('success','Product Saved as draft');
        }
        else{
            $product->status='1';
            $product->save();
            session()->flash('success','Product Published');
        }
        return redirect(route('products.index'));
    }


}
