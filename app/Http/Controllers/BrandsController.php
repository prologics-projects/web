<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\Http\Requests\Brands\BrandsCreateController;
use App\Http\Requests\Brands\BrandsUpdateController;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('brands.index')->with('brands',Brand::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BrandsCreateController $request)
    {        
        if($request->hasFile('image')){
            $image=$request->image->store('brands');
        }
        else{
            $image=null;
        }

        $brand=Brand::create([
            'image'=>$image,
            'about'=>$request->about,
            'name'=>$request->name,
        ]);



        session()->flash('success','Brand Created Successfully');
        return redirect(route('brands.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        return view('brands.create')->with('brand',$brand);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BrandsUpdateController $request, Brand $brand)
    {
        $data=$request->only(['name','about']);
        if($request->hasFile('image')){
            $image=$request->image->store('brands');
            $brand->deleteImage();
            $data['image']=$image;
        }
        $brand->update($data);

        session()->flash('success','Brand Updated Successfully');
        return redirect(route('brands.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        if($brand->products->count() ==0){
            $brand->deleteImage();
            $brand->delete();
            session()->flash('success','Brand Deleted Successfully');
        }else{
            session()->flash('error','Brand Has Some Products');
        }

        return redirect(route('brands.index'));

    }

    public function status(Brand $brand){
        if($brand->status=='1'){            
            $brand->status='0';
            $brand->save();
            session()->flash('success','Brand Saved as draft');
        }
        else{
            $brand->status='1';
            $brand->save();
            session()->flash('success','Brand Published');
        }
        return redirect(route('brands.index'));
    }


}