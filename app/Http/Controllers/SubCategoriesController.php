<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use App\Http\Requests\Categories\CategoriesCreateController;
use App\Http\Requests\Categories\CategoriesUpdateController;

class SubCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('subcategories.index')->with('subcategories',SubCategory::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subcategories.create')->with('categories',Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriesCreateController $request)
    {

        $subcategory=SubCategory::create([
            'name'=>$request->name,
            'category_id'=>$request->category_id,
        ]);

        session()->flash('success','Sub Category Created Successfully');
        return redirect(route('subcategories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategory $subcategory)
    {
        return view('subcategories.create')->with('subcategory',$subcategory)->with('categories',Category::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriesUpdateController $request, SubCategory $subcategory)
    {
        $data=$request->only(['name','category_id']);

        $subcategory->update($data);

        session()->flash('success','Sub Category Updated Successfully');
        return redirect(route('subcategories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategory $subcategory)
    {
        if($subcategory->products->count() ==0){
            $subcategory->delete();
            session()->flash('success','Sub Category Deleted Successfully');
        }else{
            session()->flash('error','Sub Category Has Some Products');
        }

        return redirect(route('subcategories.index'));

    }




}
