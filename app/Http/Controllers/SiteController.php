<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\Category;
use App\SubCategory;
use App\Product;
use App\Launc;
use App\Album;
use App\Gallery;
use App\Blog;
use Illuminate\Support\Facades\Input;

class SiteController extends Controller
{
    public function index(){
        return view('welcome')
        ->with('launch',Launc::first());
    }

    public function homepage(){
        return view('site.homepage');
    }

    
    public function galleryshow(){
        return view('site.galleryshow')
        ->with('albums',Album::orderBy('hits','desc')->get());
    }
    public function albumshow(Album $album){
        return view('site.albumshow')
        ->with('album',$album)
        ->with('images',Gallery::where('album_id',$album->id)->get());
    }
    
    public function product(){
        return view('site.product')
        ->with('brands',Brand::all())
        ->with('products',Product::all()->where('status',1))
        ->with('categories',Category::all());
    }

    public function blog(){
        $blogs = Blog::all();
        return view('site.blog')
        ->with('blogs',$blogs);
    }

    public function thisyear(){
        $year = ('2021');
        $blogs = Blog::whereYear('published_at', '=', $year)->get();
        $data = array(
            'year'=>'2021',
            );
        return view('site.blog')
        ->with('blogs',$blogs);
    }
    public function pastyear(){
        $years = ('2020');
        $blogs = Blog::whereYear('published_at', '=', $years)->get();
        return view('site.blog')
        ->with('blogs',$blogs);
    }
    public function nextyear(){
        $year = ('2022');
        $blogs = Blog::whereYear('published_at', '=', $year)->get();
        return view('site.blog')
        ->with('blogs',$blogs);
    }
    public function jan($blog){
        $year = $blog;
        $month = ('01');
        $blogs = Blog::whereYear('published_at', '=', $year)
        ->whereMonth('published_at', '=', $month)
               ->get();
        return view('site.blog')
        ->with('blogs',$blogs);
    }
    public function feb($blog){
        $year = $blog;
        $month = ('02');
        $blogs = Blog::whereYear('published_at', '=', $year)
        ->whereMonth('published_at', '=', $month)
               ->get();
        return view('site.blog')
        ->with('blogs',$blogs);
    }
    public function mar($blog){
        $year = $blog;
        $month = ('03');
        $blogs = Blog::whereYear('published_at', '=', $year)
        ->whereMonth('published_at', '=', $month)
               ->get();
        return view('site.blog')
        ->with('blogs',$blogs);
    }
    public function apr($blog){
        $year = $blog;
        $month = ('04');
        $blogs = Blog::whereYear('published_at', '=', $year)
        ->whereMonth('published_at', '=', $month)
               ->get();
        return view('site.blog')
        ->with('blogs',$blogs);
    }
    public function may($blog){
        $year = $blog;
        $month = ('05');
        $blogs = Blog::whereYear('published_at', '=', $year)
        ->whereMonth('published_at', '=', $month)
               ->get();
        return view('site.blog')
        ->with('blogs',$blogs);
    }
    public function jun($blog){
        $year = $blog;
        $month = ('06');
        $blogs = Blog::whereYear('published_at', '=', $year)
        ->whereMonth('published_at', '=', $month)
               ->get();
        return view('site.blog')
        ->with('blogs',$blogs);
    }
    public function jul($blog){
        $year = $blog;
        $month = ('07');
        $blogs = Blog::whereYear('published_at', '=', $year)
        ->whereMonth('published_at', '=', $month)
               ->get();
        return view('site.blog')
        ->with('blogs',$blogs);
    }
    public function aug($blog){
        $year = $blog;
        $month = ('08');
        $blogs = Blog::whereYear('published_at', '=', $year)
        ->whereMonth('published_at', '=', $month)
               ->get();
        return view('site.blog')
        ->with('blogs',$blogs);
    }
    public function sep($blog){
        $year = $blog;
        $month = ('09');
        $blogs = Blog::whereYear('published_at', '=', $year)
        ->whereMonth('published_at', '=', $month)
               ->get();
        return view('site.blog')
        ->with('blogs',$blogs);
    }
    public function oct($blog){
        $year = $blog;
        $month = ('10');
        $blogs = Blog::whereYear('published_at', '=', $year)
        ->whereMonth('published_at', '=', $month)
               ->get();
        return view('site.blog')
        ->with('blogs',$blogs);
    }
    public function nov($blog){
        $year = $blog;
        $month = ('11');
        $blogs = Blog::whereYear('published_at', '=', $year)
        ->whereMonth('published_at', '=', $month)
               ->get();
        return view('site.blog')
        ->with('blogs',$blogs);
    }
    public function dec($blog){
        $year = $blog;
        $month = ('12');
        $blogs = Blog::whereYear('published_at', '=', $year)
        ->whereMonth('published_at', '=', $month)
               ->get();
        return view('site.blog')
        ->with('blogs',$blogs);
    }

    public function view($blog){
        $blogs = Blog::where('id', '=', $blog)->get();
        return view('site.view')
        ->with('blogs',$blogs);
        
    }
    
    public function rnd(){
        return view('site.rnd');
    }
    
    public function story(){
        return view('site.story');
    }
    
    public function contact(){
        return view('site.contact');
    }
        
    public function oem(){
        return view('site.oem');
    }

    public function contactmail(Request $request){
        $details=[
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'email'=>$request->email,
            'telephone'=>$request->telephone,
            'subject'=>$request->subject,
            'message'=>$request->message,
        ];
        \Mail::to('web@dreamron.com')->send(new \App\Mail\ContactMail($details));
        \Mail::to($details['email'] )->send(new \App\Mail\ContactResponseMail($details));

        
        return view('site.contact')
        ->with('details',$details);
    }
    

    public function loadproducts(Request $request) {
        
        $brand_id=$request->brand_id;
        $category_id=$request->category_id;
        $subcategory_id=$request->subcategory_id;
        if(!isset($category_id) && !isset($subcategory_id)){
            $data=Product::where('status',1)->where('brand_id',$brand_id)->get();
        }
        if(!isset($brand_id) && !isset($subcategory_id)){
            $data=Product::where('status',1)->where('category_id',$category_id)->get();
        }
        if(!isset($brand_id) && !isset($category_id)){
            $data=Product::where('status',1)->where('subcategory_id',$subcategory_id)->get();
        }

        if(count($data)>0){
        ?>
        
        <div class="unique-product-container">
        <button class="navbar-toggler">
            <div id="nav-icon-prod" class="nav-icon open">
                <div class="nav-icon-inner">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </button>
        <div id="carouselExampleControls" class="carousel-product carousel slide h-100" data-ride="carousel">
            <div class="carousel-inner h-100">
                <?php foreach($data as $product){ ?>
                <div class="productcarousel carousel-item h-100" data-slide="<?php echo $product->id; ?>" >
                    <div class="row carousel-product-inner">
                        <div class="col-12 col-md-5 carousel-img">
                            <img class="d-block w-100" src="<?php echo asset('storage/'.$product->image); ?>" alt="First slide">
                        </div>
                        <div class="col-12 col-md-7 carousel-disc" >
                            <h3><?php echo $product->name; ?></h3>

                            <p><?php echo $product->description; ?></p>

                            <div class="product-info">
                                            <!--
                                    <span class="product-info-group">
                                        <span class="product-info-label">Amount</span>
                                        <span class="product-info-value"><?php echo $product->amount; ?></span>
                                    </span>
                                    <span class="product-info-group">
                                        <span class="product-info-label">SKU</span>
                                        <span class="product-info-value"><?php echo $product->sku; ?></span>
                                    </span>
                -->
                                    <span class="product-info-group">
                                        <span class="product-info-label">Category</span>
                                        <span class="product-info-value"><?php echo $product->category->name; ?></span>
                                    </span>
                                    <?php if(isset($product->subcategory)){ ?>
                                        <span class="product-info-group">
                                            <span class="product-info-label">Sub Category</span>
                                            <span class="product-info-value"><?php echo $product->subcategory->name; ?></span>
                                        </span>
                                    <?php } ?>
                            </div>

                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        </div>
        <div class="row all-product-container">
            <?php foreach($data as $product){ ?>

                <div class="col col-sm-6 col-md-4 col-lg-3 a-product productclick" data-item="<?php echo $product->id; ?>">
                    <div class="product-inner  " >
                        <img src="<?php echo asset('storage/'.$product->image); ?>" class="" alt="product name" style="width: 120px">

                        <p class="product-name"><?php echo $product->name; ?></p>

                        <!--<p>Rs. <?php echo $product->amount; ?>/=</p>-->
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="main-progress-bar all-products-footer">
            <div class="brand-bar bar-left"></div>
            <div class="brand-bar bar-right"></div>
        </div>
        <?php }
        else{
            ?>
            <div style=" 
                height: calc(100vh - 70px);
                overflow: auto;margin: 0 auto; 
                display: flex;
                align-items: center;
                justify-content: center;
                flex-direction: column;">
                <h1 class="" style="color:#D11875 ; font-size:520%">No Products Available.</h1>
            </div>
            <?php
        }

    }

    public function search(){
        $q = Input::get ( 'searchtext' );
        $data = Product::where ( 'name', 'LIKE', '%' . $q . '%' )
        ->orWhere ( 'description', 'LIKE', '%' . $q . '%' )
        ->where('status','<>','0')
        ->get ();
        if ($data->count() > 0) {
            return view('site.product')
            ->with('brands',Brand::all())
            ->with('products',$data)
            ->with('categories',Category::all());
        } else {
            return view('site.product')
            ->with('brands',Brand::all())
            ->with('products',0)
            ->with('categories',Category::all())
            ->with ( 'emptymessage','No Products find on "'.$q.'" Try to search again !' );
        }

    }

    
    public function launched(){
        $launch=Launc::first();
    
        $launch->Update([
            'status'=>1,
        ]);
    }
}
