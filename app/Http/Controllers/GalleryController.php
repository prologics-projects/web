<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Album;
use App\Gallery;
use Illuminate\Support\Facades\Auth;

class GalleryController extends Controller
{
    public function images(Album $album)
    {
        return view('albums.images')
        ->with('album',$album)
        ->with('files',Gallery::all()->where('album_id',$album->id));
    }

    function upload(Request $request)
    {
        $gallery=Gallery::create([
            'album_id'=>$request->album_id,
        ]);
        $image = $request->file('file');
        $imageName = time().$gallery->id.$request->name.'.'.$request->file->getClientOriginalExtension();
        #$imageName = time().$request->file->getClientOriginalName();
        $image->move(public_path('images/gallery/'), $imageName);
        $gallery->update([
            'image'=>$imageName,
        ]);
        
        return response()->json(['success' => $imageName]);
    }

    function fetch(Request $request)
    {
        $gallery=Gallery::all()->where('album_id',$request->album_id);

        $output = '<div class="nk-files-list">';
        foreach($gallery as $image)
            {
            $ext = explode('.', trim($image->image))[1];
            $output .= '
            <div class="col-md-3">
                <div class="card mb-4" style="width:100%; min-width:200px;">
                    <div class="view overlay">
                    <img class="card-img-top" src="'.asset('images/gallery/' . $image->image).'"
                    alt="Card image cap" style="width:100%; min-width:200px; height:250px">
                    <div class="mask rgba-white-slight "></div>
                    </div>
                
                    <div class="card-body">
                        <button type="button" class="btn btn-primary btn-sm remove_image float-right" id="'.$image->id.'"><em class="icon ni ni-trash"></em></button>
                    </div>
                </div>
            </div>
            ';
        }
        $output .= '</div>';
        echo $output;


        

    }

    function delete(Request $request)
    {
            $del_id=$request->id;
            $file=Gallery::where('id', $del_id)->first();
            $del_file=$file->image;
            Gallery::where('id', $del_id)->delete();
            \File::delete(public_path('images/gallery/'. $del_file));
    }

    function getfile(File $id){
        return response()->download(public_path('/images/gallery/'. $id->file_path));

    }

    
}
