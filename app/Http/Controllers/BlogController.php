<?php

namespace App\Http\Controllers;

use App\Category;
use App\Blog;
use Illuminate\Http\Request;
use App\Http\Requests\Blog\CreateBlogRequest;
use App\Http\Requests\Blog\UpdateBlogRequest;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('blog.index')->with('blogs',Blog::limit(5)
                ->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create')->with('categories',Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBlogRequest $request)
    {
        //dd($request);

        $image=$request->image->store('posts');

        if(empty($request->published_at)){
            $request->published_at=date('yy-m-d');
        }

        Blog::create([
            'category_id'=>$request->category,
            'image'=>$image,
            'description'=>$request->description,
            'content'=>$request->content,
            'author'=>$request->author,
            'title'=>$request->title,
            'status'=>'1',
            'published_at'=>$request->published_at,
        ]);

        session()->flash('success','Post Created Successfully');
        return redirect(route('blog.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(blog $blog)
    {
        return view('blog.create')->with('blog',$blog)->with('categories',Category::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBlogRequest $request, Blog $blog)
    {
        if(empty($request->published_at)){
            $request->published_at=date('yyyy-mm-dd');
        }
        
        $data=$request->only(['title','description','published_at','author','content','category']);
        if($request->hasFile('image')){
            $image=$request->image->store('posts');
            $blog->deleteImage();
            $data['image']=$image;
        }
        
        $blog->update($data);

        session()->flash('success','Post Updated Successfully');
        return redirect(route('blog.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        $blog->deleteImage();
        $blog->delete();

        session()->flash('success','Post Deleted Successfully');
        return redirect(route('blog.index'));
    }
}
