<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Album\CreateAlbumRequest;
use App\Http\Requests\Album\UpdateAlbumRequest;
use App\Album;
use Yajra\DataTables\Facades\DataTables;

class AlbumController extends Controller
{

    public function index(){
        return view('albums.index');
    }

    public function getdata(Request $request){
        if ($request->ajax()) {
            return Datatables::of(Album::orderBy('hits', 'desc')->get())
            ->addIndexColumn()
            ->setRowId('{{$id}}')
            ->removeColumn('updated_at')
            ->addColumn('images', function ($model) {
                return $model->galleries->count(); })
            ->addColumn('action', function ($model) {
                return view('albums.includes.actions', ['model' => $model]); })
            ->make(true);
        }
    }

    public function add(CreateAlbumRequest $request){
        $albums=Album::create([
            'name'=>$request->name,
            'description'=>$request->description,
            'hits'=>$request->hits,
        ]);

        if($request->hasFile('image')){
            $file = $request->file('image');
            $fileName = time().$request->name.'.'.$request->image->getClientOriginalExtension();
            $file->move(public_path('storage/album_cover'), $fileName);

            $albums->update([
                'image'=>'album_cover/'.$fileName,
            ]);
        }
        $toast = array('message' => 'Album Created Succesfully!');
        return response()->json($toast);
    }

    
    public function edit(UpdateAlbumRequest $request){
        $albums=Album::find($request->edit_id);
        $albums->update([
            'name' => $request->edit_name,
            'description'=>$request->edit_description,
            'hits'=>$request->edit_hits,
        ]);
        if($request->hasFile('edit_image')){
            $file = $request->file('edit_image');
            $fileName = time().$request->edit_name.'.'.$request->edit_image->getClientOriginalExtension();
            $file->move(public_path('storage/album_cover'), $fileName);
            
            # \File::delete(public_path('storage/'.$albums->image));
            $albums->update([
                'image'=>'album_cover/'.$fileName,
            ]);
        }
        $toast = array('message' => 'Album Updated Succesfully!');
        return response()->json($toast);
    }

    
    public function delete(Request $request)
    {
        $albums=Album::find($request->id);
        if($albums->galleries->count() >0 ){
            foreach($albums->galleries as $image){
                \File::delete(public_path('images/gallery/'. $image->image));
                $image->delete();
            }
        }
        Album::where('id', $request->id)->delete();

        $toast = array('message' => 'Album Deleted Succesfully!');
        return response()->json($toast);
    }
}