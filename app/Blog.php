<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Blog extends Model
{
    use SoftDeletes;
    
    protected $fillable=[
        'category_id', 'image','description','content','author','title','status','published_at'
    ];
}
