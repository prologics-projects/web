<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Brand extends Model
{

    protected $fillable=[
        'name', 'image','about'
    ];


    public function deleteImage(){
        Storage::delete($this->image);
    }

    public function products(){
        return $this->hasMany(Product::class);
    }
}
