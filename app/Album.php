<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $fillable=[
        'name', 'description','image','hits'
    ];
    
    public function galleries(){
        return $this->hasMany(Gallery::class);
    }
}
